import Permutation from './permutation'

describe('permutation with sort', () => {
    it(' abcda and acbad return true', () => {
        //given
        const testSubject = new Permutation();
        //when
        const result = testSubject.checkWithSort('abcda', 'acbad');
        //then
        expect(result).toBe(true);
    });
    it(' abcda and acbaad return false', () => {
        //given
        const testSubject = new Permutation();
        //when
        const result = testSubject.checkWithSort('abcda', 'acbaad');
        //then
        expect(result).toBe(false);
    });
});

describe('permutation with array', () => {
    it(' abcda and acbad return true', () => {
        //given
        const testSubject = new Permutation();
        //when
        const result = testSubject.checkWithArray('abcda', 'acbad');
        //then
        expect(result).toBe(true);
    });
    it(' abcda and acbaad return false', () => {
        //given
        const testSubject = new Permutation();
        //when
        const result = testSubject.checkWithArray('abcda', 'acbaad');
        //then
        expect(result).toBe(false);
    });
});