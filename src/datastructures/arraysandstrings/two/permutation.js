export default class Permutation {

    _isSameSize(left, right) {
        return left.length === right.length;
    }

    checkWithSort(left, right) {
        if(!this._isSameSize(left, right)) {
            return false;
        }
        return left.split('').sort().join('') === right.split('').sort().join('');
    }

    checkWithArray(left, right) {
        if(!this._isSameSize(left, right)) {
            return false;
        }
        const result = Array(128).fill(0);
        left.split('').forEach((char) => {
            result[char.charCodeAt()] += 1;
        });
        let isPermutation = right.split('').every((char) => {
            if (result[char.charCodeAt()] <= 0) {
                return false;
            } else {
                result[char.charCodeAt()] -= 1;
                return true;
            }
        });
        return isPermutation && result.filter((value) => value > 0).length === 0;
    }
}