export default class Unique {

    constructor() {

    }

    checkWithDataStructureWithArray(input) {
        const inputs = input.split('');
        const result = [];
        for (let index in inputs) {
            if (result.indexOf(inputs[index]) >= 0) {
                return false;
            }
            result.push(inputs[index]);
        }
        return true;
    }

    checkWithDataStructureWithSet(input) {
        const check = new Set();
        const inputs = input.split('');
        return inputs.every((current) => {
            if (check.has(current)) {
                return false;
            } else {
                check.add(current);
                return true;
            }
        });
    }

    checkWithDataStructureWithReplaceRegex(input) {
        const inputs = input.split('');
        const size = inputs.length;
        return inputs.every((current) => {
            return input.replace(new RegExp(current, 'g'), '').length + 1 === size;
        })
    }

    checkWithDataStructureWithReplaceSplitJoin(input) {
        const inputs = input.split('');
        const size = inputs.length;
        return inputs.every((current) => {
            return input.split(current).join('').length + 1 === size;
        })
    }

    checkWithDataStructureWithAsci(input) {
        if (input.length > 256) {
            return false;
        }
        const asci = new Array(256);
        const inputs = input.split('');
        return inputs.every((current) => {
            if(asci[current] === true) {
                return false;
            }
            asci[current] = true;
            return true;
        })
    }

    checkWithoutDataStructureByFilter(input) {
        const inputs = input.split('');
        const result = [];
        for (let index in inputs) {
            if (inputs.filter((value) => value === inputs[index]).length > 1) {
                return false;
            }
        }
        return true;
    }

    checkWithoutDataStructureByFilterAndEntries(input) {
        const inputs = input.split('');
        for (let [index, input] of inputs.entries()) {
            if (inputs.filter((value) => input === value).length > 1) {
                return false;
            }
        }
        return true;
    }

    checkWithoutDataStructureByFilterAndSome(input) {
        const inputs = input.split('');
        //some return first true
        return !inputs.some((currentValue) => {
            return inputs.filter((value) => value === currentValue).length > 1
        });
    }

    checkWithoutDataStructureByFilterAndEvery(input) {
        const inputs = input.split('');
        //every return first false
        return inputs.every((current) => {
            return inputs.filter((value) => value === current).length === 1
        })
    }

    checkWithoutDataStructure(input) {
        const inputs = input.split('');
        for (let currentIndex = 0; currentIndex < inputs.length; currentIndex++) {
            for (let index = currentIndex + 1; index < inputs.length; index++) {
                if (inputs[currentIndex] === inputs[index]) {
                    return false;
                }
            }
        }
        return true;
    }
}