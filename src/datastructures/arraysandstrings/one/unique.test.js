import Unique from './unique';

describe('checkWithDataStructure', () => {
    describe('with array', () => {
        test('should return false for not unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithDataStructureWithArray('abcdb')).toBe(false);
        });
        test('should return true for unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithDataStructureWithArray('abcdu123%')).toBe(true);
        });
    });
    describe('with replace', () => {
        describe('regex', ()=> {
            test('should return false for not unique string', () => {
                const unique = new Unique();
                expect(unique.checkWithDataStructureWithReplaceRegex('abcdb')).toBe(false);
            });
            test('should return true for unique string', () => {
                const unique = new Unique();
                expect(unique.checkWithDataStructureWithReplaceRegex('abcdu123%')).toBe(true);
            });
        });
        describe('split join', ()=> {
            test('should return false for not unique string', () => {
                const unique = new Unique();
                expect(unique.checkWithDataStructureWithReplaceSplitJoin('abcdb')).toBe(false);
            });
            test('should return true for unique string', () => {
                const unique = new Unique();
                expect(unique.checkWithDataStructureWithReplaceSplitJoin('abcdu123%')).toBe(true);
            });
        });
    });
    describe('with set', () => {
        test('should return false for not unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithDataStructureWithSet('abcdb')).toBe(false);
        });
        test('should return true for unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithDataStructureWithSet('abcdu123%')).toBe(true);
        });
    });

    describe('with asci', () => {
        test('should return false for not unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithDataStructureWithAsci('abcdb')).toBe(false);
        });
        test('should return true for unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithDataStructureWithAsci('abcdu123%')).toBe(true);
        });
    });
});
describe('checkWithoutDataStructure', () => {
    describe('use filter and loop', () => {
        test('should return false for not unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithoutDataStructureByFilter('abcdb')).toBe(false);
        });
        test('should return true for unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithoutDataStructureByFilter('abcdu123%')).toBe(true);
        });
    });

    describe('use filter and some', () => {
        test('should return false for not unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithoutDataStructureByFilterAndSome('abcdb')).toBe(false);
        });
        test('should return true for unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithoutDataStructureByFilterAndSome('abcdu123%')).toBe(true);
        });
    });
    describe('use filter and every', () => {
        test('should return false for not unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithoutDataStructureByFilterAndEvery('abcdb')).toBe(false);
        });
        test('should return true for unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithoutDataStructureByFilterAndEvery('abcdu123%')).toBe(true);
        });
    });
    describe('use filter and entries', () => {
        test('should return false for not unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithoutDataStructureByFilterAndEntries('abcdb')).toBe(false);
        });
        test('should return true for unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithoutDataStructureByFilterAndEntries('abcdu123%')).toBe(true);
        });
    });
    describe('use for loop', () => {
        test('should return false for not unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithoutDataStructure('abcdb')).toBe(false);
        });
        test('should return true for unique string', () => {
            const unique = new Unique();
            expect(unique.checkWithoutDataStructure('abcdu123%')).toBe(true);
        });
    });
});

