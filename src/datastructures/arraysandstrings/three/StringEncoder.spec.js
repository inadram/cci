import StringEncoder from './StringEncoder'

describe('string encoder', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new StringEncoder();
    });

    it('should encode to correct value with regex', () => {
        //given
        const expectedResult = 'am%20i%20r';
        //when
        const result = testSubject.encodeWithRegex('am i r');
        //then
        expect(result).toBe(expectedResult);
    });
    it('should encode to correct value with split join', () => {
        //given
        const expectedResult = 'a%20mi%20r';
        //when
        const result = testSubject.encodeWithSplitJoin('a mi r');
        //then
        expect(result).toBe(expectedResult);
    });

    it('should encode to correct value with loop map', () => {
        //given
        const expectedResult = 'a%20mi%20r';
        //when
        const result = testSubject.encodeWithLoopMap('a mi r');
        //then
        expect(result).toBe(expectedResult);
    });

    it('should encode to correct value with loop and build new string', () => {
        //given
        const expectedResult = 'a%20mi%20r';
        //when
        const result = testSubject.encodeWithLoopAndBuildNewString('a mi r');
        //then
        expect(result).toBe(expectedResult);
    });

    it('should encode to correct value with loop and replace to end of string', () => {
        //given
        const expectedResult = 'a%20mi%20r';
        //when
        const result = testSubject.encodeWithLoopAndReplaceToEndOfString('a mi r    ', 6);
        //then
        expect(result).toBe(expectedResult);
    });

    it('should encode to correct value with loop and replace to end of string use substr', () => {
        //given
        const expectedResult = 'a%20mi%20r';
        //when
        const result = testSubject.encodeWithLoopAndReplaceToEndOfStringBySubstr('a mi r    ', 6);
        //then
        expect(result).toBe(expectedResult);
    });

    it('should encode to correct value with loop and replace string use substr', () => {
        //given
        const expectedResult = 'a%20mi%20r';
        //when
        const result = testSubject.encodeWithLoopAndReplaceBySubstr('a mi r    ', 6);
        //then
        expect(result).toBe(expectedResult);
    });
});