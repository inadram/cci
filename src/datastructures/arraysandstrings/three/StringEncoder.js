const StringEncoder = () => {
    const encodeWithRegex = function (input) {
        return input.replace(new RegExp(/ /g), '%20');
    };

    function encodeWithSplitJoin(input) {
        return input.split(' ').join('%20');
    }

    function encodeWithLoopMap(input) {
        return input.split('').map((char) => {
            if (char === ' ') {
                return '%20';
            } else {
                return char;
            }
        }).join('');
    }

    function encodeWithLoopAndBuildNewString(input) {
        let result = '';
        input.split('').forEach((value) => {
            if (value === ' ') {
                result += '%20';
            } else {
                result += value;
            }
        });
        return result;
    }

    function encodeWithLoopAndReplaceToEndOfString(input, sizeOfString) {
        let lastIndex = input.length - 1;
        input = input.split('');
        for (let index = sizeOfString - 1; index >= 0; index--) {
            if (input[index] === ' ') {
                input[lastIndex] = '0';
                input[lastIndex - 1] = '2';
                input[lastIndex - 2] = '%';
                lastIndex -= 3;
            } else {
                input[lastIndex] = input[index];
                lastIndex -= 1;
            }
        }
        return input.join('');
    }

    function encodeWithLoopAndReplaceToEndOfStringBySubstr(input, sizeOfString) {
        let lastIndex = input.length - 1;
        for (let index = sizeOfString - 1; index >= 0; index--) {
            if (input.charAt(index) === ' ') {
                input = replaceAt(input, lastIndex, '0');
                input = replaceAt(input, lastIndex - 1, '2');
                input = replaceAt(input, lastIndex - 2, '%');
                lastIndex -= 3;
            } else {
                input = replaceAt(input, lastIndex, input.charAt(index));
                lastIndex -= 1;
            }
        }
        return input;
    }

    function encodeWithLoopAndReplaceBySubstr(input) {
        for (let index = 0; index < input.length; index++) {
            if (input.charAt(index) === ' ') {
                input = replaceAt(input, index, '%');
                index++;
                input = shift(input, index);
                input = replaceAt(input, index, '2');
                index++;
                input = shift(input, index);
                input = replaceAt(input, index, '0');
            }
        }
        return input;
    }

    function shift(input, index) {
        for (let i = input.length - 1; i > index; i--) {
            input = replaceAt(input, i, input.charAt(i - 1));
        }
        return input;
    }

    function replaceAt(input, index, value) {
        return input.substr(0, index) + value + input.substr(index + 1, input.length);
    }

    return {
        encodeWithRegex: encodeWithRegex,
        encodeWithSplitJoin: encodeWithSplitJoin,
        encodeWithLoopMap: encodeWithLoopMap,
        encodeWithLoopAndBuildNewString: encodeWithLoopAndBuildNewString,
        encodeWithLoopAndReplaceToEndOfString: encodeWithLoopAndReplaceToEndOfString,
        encodeWithLoopAndReplaceToEndOfStringBySubstr: encodeWithLoopAndReplaceToEndOfStringBySubstr,
        encodeWithLoopAndReplaceBySubstr: encodeWithLoopAndReplaceBySubstr
    }
};

export default StringEncoder;