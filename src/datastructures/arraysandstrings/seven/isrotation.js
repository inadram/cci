const IsRotation = (str1, str2) => {
    const run = () => {
        return str1.length === str2.length && (str1 + str2).includes(str1);
    };

    function byFirstIndexString() {
        return str1.length === str2.length && str2 === build(str1, str2);
    }

    function build(left, right) {
        let firstIndex = findFirstIndex(left, right);
        return left.slice(firstIndex) + left.slice(0, firstIndex);
    }

    //amira
    //iraam
    function findFirstIndex(left, right) {
        for (let index = 0; index < left.length; index++) {
            if (right.charAt(0) === left.charAt(index) && left.slice(index) === right.slice(0, left.length - index)) {
                return index;
            }
        }
    }

    function byFirstIndexArray() {
        if (str1.length !== str2.length) {
            return false;
        }
        str1 = str1.split('');
        str2 = str2.split('');

        for (let i = 0; i < str1.length; i++) {
            if (str1[i] === str2[0] &&
                str1.slice(i).every((val, id) => val === str2[id])) {
                return str1.slice(i).concat(str1.slice(0, i)).every((value, id) => value === str2[id]);
            }
        }
        return false;
    }

    return {run, byFirstIndexString, byFirstIndexArray};
};

export default IsRotation;