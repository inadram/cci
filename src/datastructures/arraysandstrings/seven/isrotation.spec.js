import IsRotation from './isrotation'
describe('Rotation', ()=> {
    let testSubject;
    const str1 = 'waterbottLe';
    const str2 = 'erbottLewat';
    beforeEach(()=>{
        testSubject = new IsRotation(str1,str2);
    });
    it('run', ()=> {
        //when
       const result = testSubject.run(str1, str2);
       //then
        expect(result).toBe(true);
    });

    it('by first index string', ()=> {
        //when
        const result = testSubject.byFirstIndexString(str1, str2);
        //then
        expect(result).toBe(true);
    });

    it('by first index array', ()=> {
        //when
        const result = testSubject.byFirstIndexArray(str1, str2);
        //then
        expect(result).toBe(true);
    });
});