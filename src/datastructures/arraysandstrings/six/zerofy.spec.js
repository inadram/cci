import Zerofy from './zerofy'

describe('zerofy', () => {

    let input, expected, testSubject;
    beforeEach(()=>{
        //given
        input =[
            [1,2,3,0,5],
            [0,2,3,0,5],
            [1,2,3,1,5],
            [1,2,3,1,5],
        ];
        expected = [
            [0,0,0,0,0],
            [0,0,0,0,0],
            [0,2,3,0,5],
            [0,2,3,0,5],
        ];
        testSubject = new Zerofy();
    });
    it('run', async () => {
        //when
        const result = await testSubject.run(input);
        //then
        expect(result).toEqual(expected)
    });

    it('runSetWithFill', async () => {
        //when
        const result = await testSubject.runSetWithFill(input);
        //then
        expect(result).toEqual(expected)
    });

    it('runWithArray', ()=> {
       //when
       const result = testSubject.runWithArray(input);
       //then
        return expect(result).resolves.toEqual(expected);
    });
});