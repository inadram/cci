const Zerofy = () => {
    function _run(input) {
        const rows = new Set();
        const cols = new Set();
        input.forEach((row, rowIndex) => row.forEach((col, colIndex) => {
            if (col === 0) {
                rows.add(rowIndex);
                cols.add(colIndex);
            }
        }));
        for (let row = 0; row < input.length; row++) {
            for (let col = 0; col < input[row].length; col++) {
                if (rows.has(row) || cols.has(col)) {
                    input[row][col] = 0;
                }
            }
        }
        return input;
    }

    const _runWithArray = async (input) => {
        const rows = [];
        const cols = [];
        for (let row = 0; row < input.length; row++) {
            for (let col = 0; col < input[row].length; col++) {
                if (input[row][col] === 0) {
                    rows.push(row);
                    cols.push(col);
                    break;
                }
            }
        }
        for (let row = 0; row < input.length; row++) {
            for (let col = 0; col < input[row].length; col++) {
                if (rows.some(value => value === row) || cols.some(value => value === col)) {
                    input[row][col] = 0;
                }
            }
        }
        return input;
    };

    function _runSetWithFill(input) {
        const rows = new Set();
        const cols = new Set();
        for (let row = 0; row < input.length; row++) {
            for (let col = 0; col < input[row].length; col++) {
                if (input[row][col] === 0) {
                    cols.add(col);
                    rows.add(row);
                    break;
                }
            }
        }

        rows.forEach(row => input[row].fill(0));
        cols.forEach(col => input
            .filter(row => !rows.has(row))
            .forEach((row, rowIndex) => input[rowIndex][col] = 0));
        return input;
    }

    return {
        run: input => Promise.resolve(_run(input)),
        runWithArray: input => _runWithArray(input),
        runSetWithFill: input => _runSetWithFill(input)
    }
};

export default Zerofy;