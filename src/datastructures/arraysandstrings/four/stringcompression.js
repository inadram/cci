let StringCompression = () => {
};

StringCompression.prototype.compress = function (input) {
    input = input.split('');
    let current = input[0];
    let count = 0;
    let result = '';
    for (let index = 0; index < input.length; index++) {
        if (current === input[index]) {
            count++;
        } else {
            result += current + count;
            current = input[index];
            count = 1;
        }
    }
    result += current + count;
    return (result.length > input.length) ? input.join('') : result;
};

StringCompression.prototype.compressJoin = function (input) {
    let current = input.charAt(0);
    let count = 0;
    let result = [];
    for (let index = 0; index < input.length; index++) {
        if (input.charAt(index) === current) {
            count++;
        } else {
            result.push(current);
            result.push(count);
            current = input.charAt(index);
            count = 1;
        }
    }
    result.push(current);
    result.push(count);
    return (result.length > input.length) ? input : result.join('');
};

StringCompression.prototype.compressMap = function (input) {
    let map = new Map();
    input.split('').forEach((char) => {
        if (map.has(char)) {
            let value = map.get(char);
            map.set(char, value + 1)
        } else {
            map.set(char, 1);
        }
    });
    let result = '';
    // map.forEach((value, key) => result += key + value);
    return [...map].map((value)=>{return value.join('')}).join('');
};

export default StringCompression;