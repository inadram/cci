import StringCompression from './stringcompression';

describe('compress', () => {
    describe('bigger', () => {
        it('return same if it get bigger', () => {
            //given
            const input = 'some input';
            const testSubject = new StringCompression();
            //when
            const result = testSubject.compress(input);
            //then
            expect(result).toBe(input);
        });
    });

    describe('smaller', () => {
        describe('string concat', () => {
            it('return compress for aaabbaac', () => {
                //given
                const input = 'aaabbaac';
                const testSubject = new StringCompression();
                const expectedResult = 'a3b2a2c1';
                //when
                const result = testSubject.compress(input);
                //then
                expect(result).toBe(expectedResult);
            });
            it('return compress for aaabbaacc', () => {
                //given
                const input = 'aaabbaacc';
                const testSubject = new StringCompression();
                const expectedResult = 'a3b2a2c2';
                //when
                const result = testSubject.compress(input);
                //then
                expect(result).toBe(expectedResult);
            });
            it('return compress for accc', () => {
                //given
                const input = 'accc';
                const testSubject = new StringCompression();
                const expectedResult = 'a1c3';
                //when
                const result = testSubject.compress(input);
                //then
                expect(result).toBe(expectedResult);
            });
            it('return compress for accc', () => {
                //given
                const input = 'aaac';
                const testSubject = new StringCompression();
                const expectedResult = 'a3c1';
                //when
                const result = testSubject.compress(input);
                //then
                expect(result).toBe(expectedResult);
            });
            it('return compress for accc', () => {
                //given
                const input = 'aaa';
                const testSubject = new StringCompression();
                const expectedResult = 'a3';
                //when
                const result = testSubject.compress(input);
                //then
                expect(result).toBe(expectedResult);
            });
        });
        describe('array join', () => {
            it('return compress for aaabbaac', () => {
                //given
                const input = 'aaabbaac';
                const testSubject = new StringCompression();
                const expectedResult = 'a3b2a2c1';
                //when
                const result = testSubject.compressJoin(input);
                //then
                expect(result).toBe(expectedResult);
            });
        });
        describe('set and omit dups', () => {
            it('return compress for aaabbaac', () => {
                //given
                const input = 'aaabbaac';
                const testSubject = new StringCompression();
                const expectedResult = 'a5b2c1';
                //when
                const result = testSubject.compressMap(input);
                //then
                expect(result).toBe(expectedResult);
            });
        });
    });
});