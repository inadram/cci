export default function Rotate() {
    function right(input) {
        const length = input.length;
        const lastIndex = length - 1;
        for (let layer = 0; layer < length / 2; layer++) {
            for (let col = layer; col < lastIndex - layer; col++) {
                let temp = input[layer][col];
                //left to top
                input[layer][col] = input[lastIndex - col][layer];
                //bottom to left
                input[lastIndex - col][layer] = input[lastIndex-layer][lastIndex - col];
                //right to bottom
                input[lastIndex-layer][lastIndex - col] = input[col][lastIndex-layer];
                //top to right
                input[col][lastIndex-layer] = temp;
            }
        }
        return input;
    }

    return {
        right: right
    }
}