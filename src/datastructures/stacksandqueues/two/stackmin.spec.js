import StackMin from './stackmin';

describe('stackMin', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new StackMin();
    });
    it('push and pop expected items', () => {
        // Given
        new Array(10).fill(0).forEach((value, index) => testSubject.push(index));

        for (let i = 9; i >= 0; i--) {
            // When
            let pop = testSubject.pop();

            // Then
            expect(pop).toBe(i);
        }
    });

    describe('get expected min value', () => {
        beforeEach(() => {
            testSubject.push(10);
            testSubject.push(5);
            testSubject.push(2);
            testSubject.push(20);
            testSubject.push(2);
            testSubject.push(1);
            testSubject.push(22);
        });
        it('without pop', () => {
            // When
            const result = testSubject.min();

            // Then
            expect(result).toBe(1);
        });

        it('after pop min value', () => {
            // Given
            testSubject.pop();
            testSubject.pop();
            // When
            const result = testSubject.min();

            // Then
            expect(result).toBe(2);
        });

        it('after pop dup min value', () => {
            // Given
            testSubject.pop();
            testSubject.pop();
            testSubject.pop();
            testSubject.pop();
            // When
            const result = testSubject.min();

            // Then
            expect(result).toBe(2);
        });

    });

    describe('get expected min stack value', () => {
        beforeEach(() => {
            testSubject.push(10);
            testSubject.push(5);
            testSubject.push(2);
            testSubject.push(20);
            testSubject.push(2);
            testSubject.push(1);
            testSubject.push(22);
        });
        it('without pop', () => {
            // When
            const result = testSubject.getMinStack();

            // Then
            expect(result).toBe(1);
        });

        it('after pop min value', () => {
            // Given
            testSubject.pop();
            testSubject.pop();
            // When
            const result = testSubject.getMinStack();

            // Then
            expect(result).toBe(2);
        });

        it('after pop dup min value', () => {
            // Given
            testSubject.pop();
            testSubject.pop();
            testSubject.pop();
            testSubject.pop();
            // When
            const result = testSubject.getMinStack();

            // Then
            expect(result).toBe(2);
        });

    });

    describe('get expected min stack node value', () => {
        beforeEach(() => {
            testSubject.push(10);
            testSubject.push(5);
            testSubject.push(2);
            testSubject.push(20);
            testSubject.push(2);
            testSubject.push(1);
            testSubject.push(22);
        });
        it('without pop', () => {
            // When
            const result = testSubject.getMinStackNode();

            // Then
            expect(result).toBe(1);
        });

        it('after pop min value', () => {
            // Given
            testSubject.pop();
            testSubject.pop();
            // When
            const result = testSubject.getMinStackNode();

            // Then
            expect(result).toBe(2);
        });

        it('after pop dup min value', () => {
            // Given
            testSubject.pop();
            testSubject.pop();
            testSubject.pop();
            testSubject.pop();
            // When
            const result = testSubject.getMinStackNode();

            // Then
            expect(result).toBe(2);
        });

    })
});