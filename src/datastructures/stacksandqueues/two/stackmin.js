class Node {
    constructor(value, min) {
        this.value = value;
        this.min = min;
    }
}

class StackMin {
    constructor() {
        this.stack = [];
        this.minStack = [];
        this.stackNode = [];
    }

    pop() {
        const popped = this.stack.shift();
        if (this.minStack[ 0 ] === popped) {
            this.minStack.shift();
        }
        this.stackNode.shift();
        return popped;
    }

    push(value) {
        this.stack.unshift(value);
        if (this.minStack.length === 0 || this.minStack[ 0 ] >= value) {
            this.minStack.unshift(value);
        }
        const current = this.stackNode.length > 0 ? this.stackNode[ 0 ].value : Number.MAX_VALUE;
        this.stackNode.unshift(new Node(value, Math.min(current, value)));
    }

    peek() {
        return this.stack[ 0 ];
    }

    min() {
        let minValue = Number.MAX_VALUE;
        let tempStack = [];
        while (this.peek()) {
            let popped = this.pop();
            minValue = Math.min(minValue, popped);
            tempStack.unshift(popped);
        }
        while (tempStack.length > 0) {
            this.push(tempStack.shift());
        }
        return minValue;
    }

    getMinStack() {
        return this.minStack[ 0 ];
    }

    getMinStackNode() {
        return this.stackNode[0].min;
    }
}

export default StackMin;