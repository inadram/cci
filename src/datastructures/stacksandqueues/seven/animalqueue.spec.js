import AnimalQueue from './animalqueue';
import { Cat, Dog } from './animal';

describe('Animal Queue', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new AnimalQueue();
    });

    describe('without index', () => {
        it('enqueue and dequeue', () => {
            // Given
            testSubject.enqueue(Cat);
            testSubject.enqueue(Dog);
            testSubject.enqueue(Cat);

            // when
            const result = testSubject.dequeueAny();
            const result2 = testSubject.dequeueAny();
            const result3 = testSubject.dequeueAny();

            // then
            expect(result).toBe(Cat);
            expect(result2).toBe(Dog);
            expect(result3).toBe(Cat);
        });

        it('enqueue and dequeue Dog', () => {
            // Given
            testSubject.enqueue(Cat);
            testSubject.enqueue(Dog);
            testSubject.enqueue(Cat);

            // when
            const result = testSubject.dequeueDog();

            // then
            expect(result).toBe(Dog);
        });

        it('enqueue and dequeue Cat', () => {
            // Given
            testSubject.enqueue(Dog);
            testSubject.enqueue(Dog);
            testSubject.enqueue(Dog);
            testSubject.enqueue(Cat);
            testSubject.enqueue(Dog);
            testSubject.enqueue(Cat);

            // when
            const result = testSubject.dequeueCat();

            // then
            expect(result).toBe(Cat);
        });
    });
    describe('with index', () => {
        it('enqueue and dequeue', () => {
            // Given
            testSubject.enqueue(Cat);
            testSubject.enqueue(Dog);
            testSubject.enqueue(Cat);

            // when
            const result = testSubject.dequeueAnyWithIndex();
            const result2 = testSubject.dequeueAnyWithIndex();
            const result3 = testSubject.dequeueAnyWithIndex();

            // then
            expect(result).toBe(Cat);
            expect(result2).toBe(Dog);
            expect(result3).toBe(Cat);
        });

        it('enqueue and dequeue Dog with index', () => {
            // Given
            testSubject.enqueue(Cat);
            testSubject.enqueue(Dog);
            testSubject.enqueue(Cat);

            // when
            const result = testSubject.dequeueDogWithIndex();

            // then
            expect(result).toBe(Dog);
        });

        it('enqueue and dequeue Cat with index', () => {
            // Given
            testSubject.enqueue(Dog);
            testSubject.enqueue(Dog);
            testSubject.enqueue(Dog);
            testSubject.enqueue(Cat);
            testSubject.enqueue(Dog);
            testSubject.enqueue(Cat);

            // when
            const result = testSubject.dequeueCatWithIndex();

            // then
            expect(result).toBe(Cat);
        });
    });

});