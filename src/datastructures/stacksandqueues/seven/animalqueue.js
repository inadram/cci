import { Animal, Dog, Cat } from './animal';

const AnimalQueue = function() {
    const animals = [];
    const cats = [];
    const dogs = [];
    let index = 0;
    const enqueue = (value) => {
        index += 1;
        animals.push(new Animal(value, index));
        switch (value) {
            case Cat:
                cats.push(new Animal(value, index));
                break;
            case Dog:
                dogs.push(new Animal(value, index));
                break;
        }
    };
    const dequeueAny = () => {
        return animals.shift().type;
    };

    const dequeueDog = () => {
        return deQueueAnimal(Dog);
    };

    const dequeueCat = () => {
        return deQueueAnimal(Cat);
    };

    function deQueueAnimal(animalType) {
        const tempQueue = [];
        while (animals[ 0 ].type != animalType) {
            tempQueue.push(animals.shift());
        }
        const animal = animals.shift();
        while (tempQueue.length > 0) {
            animals.push(tempQueue.shift());
        }

        return animal.type;
    }

    const dequeueAnyWithIndex = () => {
        return (dogs.length === 0 || (cats[0].index) < (dogs[0].index)) ? cats.shift().type : dogs.shift().type;
    };

    const dequeueDogWithIndex = () => {
        return dogs.shift().type;
    };

    const dequeueCatWithIndex  = () => {
        return cats.shift().type;
    };

    return {
        enqueue, dequeueAny, dequeueCat, dequeueDog, dequeueAnyWithIndex, dequeueCatWithIndex, dequeueDogWithIndex
    }
};

export default AnimalQueue;