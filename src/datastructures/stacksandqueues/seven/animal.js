export const Animal = (type, index) => {
    return {
        type, index
    }
};

export const Cat = 'CAT';
export const Dog = 'Dog';