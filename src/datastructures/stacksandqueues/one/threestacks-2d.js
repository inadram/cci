const ThreeStacks2d = function(size) {
    this.size = size;
    this.array2d = [ [] ];
};

ThreeStacks2d.prototype.push = function(index, value) {
    if (this.array2d.length - 1 < index) {
        const sizeOfArray = index - ( this.array2d.length - 1 );
        const array = new Array(sizeOfArray).fill([]);
        this.array2d = this.array2d.concat(array);
    }
    this.pushValue(index, value);
};

ThreeStacks2d.prototype.pushValue = function(index, value) {
    if (this.array2d[ index ].length === this.size) {
        let popped = this.array2d[ index ].pop();
        this.push(index + 1, popped);
    }
    this.array2d[ index ].unshift(value);
};

ThreeStacks2d.prototype.pop = function(index) {
    if (this.array2d[ index ].length === 0) {
        this.array2d.splice(index, 1);
    }
    return this.array2d[ index ].shift();
};

ThreeStacks2d.prototype.peek = function(index) {
    return this.array2d[ index ][ 0 ];
};

ThreeStacks2d.prototype.popWithShift = function(index) {
    let popped = this.pop(index);
    this.shiftFromNext(index);
    return popped;
};

ThreeStacks2d.prototype.shiftFromNext = function(index) {
    if (index < 2) {
        const popped = this.pop(index + 1);
        this.array2d[ index ].push(popped);
        this.shiftFromNext(index + 1);
    }
};

export default ThreeStacks2d;