function Threestacks(size) {
    this.size = size;
    this.singleArray = new Array(size * 3);
}

Threestacks.prototype.push = function (index, input) {
    this.singleArray.splice((index * 10), 0, input);
};
Threestacks.prototype.pop = function (index) {
    return this.singleArray.splice((index * 10), 1)[0];
};

Threestacks.prototype.popWithShift = function (index) {
    const popped = this.pop(index);
    this.shiftFromNext(index);
    return popped;
};

Threestacks.prototype.shiftFromNext = function (index) {
    if (index < 2) {
        const popped = this.pop(index + 1);
        if (this.singleArray.length > index * 10) {
            this.singleArray.splice((index * 10) + 10, popped);
        } else {
            this.singleArray.push(popped);
        }
        this.shiftFromNext(index + 1);
    }
};

Threestacks.prototype.peek = function (index) {
    return this.singleArray[index * this.size];
};

export default Threestacks;