import Threestacks from './threestacks';

describe('three stack with single array', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new Threestacks(10);
    });
    it('push/peek to first one', () => {
        //when
        testSubject.push(0, 100);

        //then
        expect(testSubject.peek(0)).toBe(100);
    });
    it('push/peek to second one', () => {
        //when
        testSubject.push(0, 100);
        testSubject.push(1, 101);

        //then
        expect(testSubject.peek(1)).toBe(101);
    });
    it('push 11 times to first one will put the first one onto second stack', () => {

        //when
        new Array(11).fill(0).forEach((value, index) => testSubject.push(0, index));

        //then
        expect(testSubject.peek(1)).toBe(0);
    });
    it('push 30 times to first stack only then by popping from second stack then third stack values should come through', () => {

        //when
        new Array(30).fill(0).forEach((value, index) => testSubject.push(0, index));

        //then
        for (let i = 19; i >= 10; i--) {
            expect(testSubject.pop(1)).toBe(i);
        }
        for (let i = 9; i >= 0; i--) {
            expect(testSubject.pop(1)).toBe(i);
        }

    });

    it('push 10 times to each stack then by popping from second stack then third stack values should come through', () => {
        //when
        new Array(30).fill(0).forEach((value, index) => testSubject.push(Math.floor(index / 10), index));
        //then
        for (let i = 19; i >= 10; i--) {
            expect(testSubject.pop(1)).toBe(i);
        }
        for (let i = 29; i >= 20; i--) {
            expect(testSubject.pop(1)).toBe(i);
        }

    });

    it('pop with shift then should get out in order', () => {
        //when
        new Array(30).fill(0).forEach((value, index) => testSubject.push(Math.floor(index / 10), index));
        //then
        for (let i = 9; i >= 0; i--) {
            expect(testSubject.popWithShift(0)).toBe(i);
        }
        for (let i = 10; i >= 19; i++) {
            expect(testSubject.popWithShift(0)).toBe(i);
        }
        for (let i = 20; i >= 29; i++) {
            expect(testSubject.popWithShift(0)).toBe(i);
        }
    });

    it('size', () => {
        //then
        expect(testSubject.size).toBe(10);
    });
});