function PlateStack(maxSize) {
    let stacks = [ [] ];

    function push(value) {
        if (stacks[ 0 ].length >= maxSize) {
            stacks.unshift([]);
        }
        stacks[ 0 ].unshift(value);
    }

    const pop = () => {
        let popped = stacks[ 0 ].shift();
        if (stacks[ 0 ].length === 0) {
            stacks.shift();
        }
        return popped;
    };

    function pushAt(index, value) {
        let diff = index - ( stacks.length - 1 );
        if (diff > 0) {
            stacks = stacks.concat(new Array(diff).fill([]))
        }
        if (stacks[ index ].length >= maxSize) {
            stacks.unshift([]);
        }

        stacks[ index ].unshift(value);
    }

    function popAt(index) {
        let popped = stacks[ index ].shift();
        if (stacks[ index ].length === 0) {
            stacks.splice(index, 1);
        }
        return popped;
    }

    const popAtWithShift = (index) => {
        let popped = popAt(index);
        shiftFromNext(index);
        return popped;
    };

    function shiftFromNext(index) {
        if (index < stacks.length - 1) {
            let popped = popAt(index + 1);
            shiftFromNext(index + 1);
            stacks[ index ].push(popped);
        }
    }

    return {
        stacks,
        push: (value) => push(value),
        pop,
        popAt,
        pushAt: (index, value) => pushAt(index, value),
        popAtWithShift: (index) => popAtWithShift(index)
    }
}

export default PlateStack;