import PlateStack from './platestack';

describe('plate of stacks', () => {
    let testSubject;
    let size = 3;
    beforeEach(() => {
        testSubject = new PlateStack(size);
    });
    it('push and pop as expected', () => {
        // Given
        new Array(5).fill(0).forEach((value, index) => testSubject.push(index));

        // When
        const result = testSubject.pop();

        // Then
        expect(result).toBe(4);
    });
    it('push and pop at index as expected', () => {
        // Given
        new Array(5).fill(0).forEach((value, index) => testSubject.pushAt(1, index));

        // When
        const result = testSubject.popAt(2);

        // Then
        expect(result).toBe(2);
    });
    it('push and pop at index with shift as expected', () => {
        // Given
        new Array(5).fill(0).forEach((value, index) => testSubject.pushAt(1, index));

        for(let i=4; i>=0; i--) {
            // When
            const result = testSubject.popAtWithShift(1);

            // Then
            expect(result).toBe(i);
        }
    });
});