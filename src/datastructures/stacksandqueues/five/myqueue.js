const MyQueue = function() {
    this.pushStack = [];
    this.peekStack = [];
};
//1,2,3
MyQueue.prototype.enqueue = function(value) {
    this.pushStack.push(value);
};

MyQueue.prototype.dequeue = function() {
    let temp = [];
    while (this.pushStack.length > 0) {
        temp.push(this.pushStack.pop())
    }
    let popped = temp.pop();
    while (temp.length > 0) {
        this.pushStack.push(temp.pop());
    }
    return popped;
};
//1,2,3
//3,2,1
MyQueue.prototype.peek = function() {
    let temp = [];
    while (this.pushStack.length > 0) {
        temp.push(this.pushStack.pop())
    }
    let peeked = temp.pop();
    temp.push(peeked);
    while (temp.length > 0) {
        this.pushStack.push(temp.pop());
    }
    return peeked;
};

MyQueue.prototype.dequeueWithBuffer = function() {
    if(this.peekStack.length > 0) {
        return this.peekStack.pop();
    } else {
        this.moveToPeekStack();
        return this.dequeueWithBuffer();
    }

};

MyQueue.prototype.peekWithBuffer = function() {
    if(this.peekStack.length > 0) {
        let peeked = this.peekStack.pop();
        this.peekStack.push(peeked);
        return peeked;
    } else  {
        this.moveToPeekStack();
        return this.peekWithBuffer();
    }

};
// 1,2,3
// 1,2,3
MyQueue.prototype.moveToPeekStack = function() {
  while (this.pushStack.length > 0) {
    this.peekStack.push(this.pushStack.pop());
  }
};

export default MyQueue;