import MyQueue from './myqueue';

describe('MyQueue', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new MyQueue();
    });
    it('enqueuee/dequeue as expected', () => {
        // Given
        testSubject.enqueue(1);
        testSubject.enqueue(2);
        testSubject.enqueue(3);

        // When
        const result = testSubject.dequeue();
        const result2 = testSubject.dequeue();

        // Then
        expect(result).toBe(1);
        expect(result2).toBe(2);
    });
    it('enqueue/peek as expected', () => {
        // Given
        testSubject.enqueue(1);
        testSubject.enqueue(2);
        testSubject.enqueue(3);

        // When
        const result = testSubject.peek();
        const result2 = testSubject.peek();

        // Then
        expect(result).toBe(1);
        expect(result2).toBe(1);
    });
});

describe('MyQueue buffer', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new MyQueue();
    });
    it('enqueue/dequeue as expected', () => {
        // Given
        testSubject.enqueue(1);
        testSubject.enqueue(2);
        testSubject.enqueue(3);

        // When
        const result = testSubject.dequeueWithBuffer();
        const result2 = testSubject.dequeueWithBuffer();

        // Then
        expect(result).toBe(1);
        expect(result2).toBe(2);
    });
    it('enqueue/peek as expected', () => {
        // Given
        testSubject.enqueue(1);
        testSubject.enqueue(2);
        testSubject.enqueue(3);

        // When
        const result = testSubject.peekWithBuffer();
        const result2 = testSubject.peekWithBuffer();

        // Then
        expect(result).toBe(1);
        expect(result2).toBe(1);
    });
});