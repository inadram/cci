import Hanoi from './hanoi';

describe('Hanoi', () => {
    let testSubject;
    it('move for one disk', () => {
        // Given
        const tower = [[1],[],[]];
        const expectedTower = [[],[],[1]];
        testSubject = new Hanoi(tower);

        // when
        const result = testSubject.move();

        // then
        expect(result).toEqual(expectedTower);
    });

    it('move for two disks', () => {
        // Given
        const tower = [[1,2],[],[]];
        const expectedTower = [[],[],[1,2]];
        testSubject = new Hanoi(tower);

        // when
        const result = testSubject.move();

        // then
        expect(result).toEqual(expectedTower);
    });

    it('move for three disks', () => {
        // Given
        const tower = [[1,2,3],[],[]];
        const expectedTower = [[],[],[1,2,3]];
        testSubject = new Hanoi(tower);

        // when
        const result = testSubject.move();

        // then
        expect(result).toEqual(expectedTower);
    });

    it('move for four disks', () => {
        // Given
        const tower = [[1,2,3,4],[],[]];
        const expectedTower = [[],[],[1,2,3,4]];
        testSubject = new Hanoi(tower);

        // when
        const result = testSubject.move();

        // then
        expect(result).toEqual(expectedTower);
    });

    it('move for five disks', () => {
        // Given
        const tower = [[1,2,3,4,5],[],[]];
        const expectedTower = [[],[],[1,2,3,4,5]];
        testSubject = new Hanoi(tower);

        // when
        const result = testSubject.move();

        // then
        expect(result).toEqual(expectedTower);
    });
});