export default class Hanoi {
    constructor(tower) {
        this.tower = tower;
    }

    move() {
        const towerOne = this.tower[ 0 ];
        const towerMiddle = this.tower[ 1 ];
        const towerThree = this.tower[ 2 ];
        this.moveMethod(towerOne, towerMiddle, towerThree);
        return this.tower;
    }

    moveMethod(one, middle, three) {
        // while tower one is not empty
        if (one.length && ( !middle.length || one[ 0 ] < middle[ 0 ] )) {
            // get disk from first tower
            let disk = one.shift();
            // move to middle
            middle.unshift(disk);

            // if third tower disk smaller do method with swap tower 1 and 3
            if (three.length > 0 && three[ 0 ] < middle[ 0 ]) {
                this.moveMethod(three, middle, one);
            }
            //get disk from middle tower
            const middleDisk = middle.shift();
            //move to third tower
            three.unshift(middleDisk);
            this.moveMethod(one, middle, three);
        }
    }

}