import SortStack from './sortstack';

describe('sort stack', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new SortStack();
    });

    it('sort two item', () => {
        // Given
        testSubject.push(10);
        testSubject.push(20);

        // when
        const result = testSubject.peek();

        // then
        expect(result).toBe(10);
    });

    it('sort five item', () => {
        // Given
        testSubject.push(10);
        testSubject.push(20);
        testSubject.push(50);
        testSubject.push(30);
        testSubject.push(100);
        testSubject.push(70);

        // when
        const result = testSubject.peek();
        const result2 = testSubject.pop();
        const result3 = testSubject.pop();
        const result4 = testSubject.pop();
        const result5 = testSubject.pop();

        // then
        expect(result).toBe(10);
        expect(result2).toBe(10);
        expect(result3).toBe(20);
        expect(result4).toBe(30);
        expect(result5).toBe(50);
    });

    it('order', () => {
        // Given
        const stack = [ 10, 40, 20, 100, 80, 90, 5 ];
        const orderedStack = [ 5, 10, 20, 40, 80, 90, 100 ];

        // when
        const result = testSubject.order(stack);

        // Then
        expect(result).toEqual(orderedStack);
    });

    it('order recursion', () => {
        // Given
        const stack = [ 10, 40, 20, 100, 80, 90, 5 ];
        const orderedStack = [ 5, 10, 20, 40, 80, 90, 100 ];

        // when
        const result = testSubject.orderRec(stack, []);

        // Then
        expect(result).toEqual(orderedStack);
    })
});