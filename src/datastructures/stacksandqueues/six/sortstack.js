const SortStack = () => {
    const tempStack = [];
    const stack = [];

    function push(value) {
        while (stack.length > 0 && value > stack[ 0 ]) {
            tempStack.unshift(stack.shift());
        }
        stack.unshift(value);
        while (tempStack.length > 0) {
            stack.unshift(tempStack.shift());
        }
    }

    function pop() {
        return stack.shift();
    }

    function peek() {
        return stack[ 0 ];
    }

    function isEmpty() {
        return stack.length === 0;
    }

    function order(stack) {
        while (stack.length > 0) {
            const current = stack.shift();
            while (tempStack.length > 0 && current > tempStack[ 0 ]) {
                stack.unshift(tempStack.shift());
            }
            tempStack.unshift(current)
        }
        return tempStack;
    }

    function orderRec(stack, temp) {
        if (stack.length > 0) {
            const current = stack.shift();
            while (temp.length > 0 && temp[ 0 ] < current) {
                stack.unshift(temp.shift());
            }
            temp.unshift(current);
            orderRec(stack, temp);
        }
        return temp;
    }

    return {
        push, pop, peek, isEmpty, order, orderRec
    }
};

export default SortStack;