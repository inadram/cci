import FindDuplicate from './findduplicate';
import LinkedList from '../lib/linkedlist';

describe('find duplicate', () => {
    let testSubject;
    let expectedUniqueLinkedList;
    beforeEach(() => {
        const linkedList = new LinkedList(10);
        linkedList.append(10);
        linkedList.append(5);
        linkedList.append(10);
        linkedList.append(20);
        linkedList.append(30);
        linkedList.append(10);
        testSubject = new FindDuplicate(linkedList);
        expectedUniqueLinkedList = new LinkedList(10);
        expectedUniqueLinkedList.append(5);
        expectedUniqueLinkedList.append(20);
        expectedUniqueLinkedList.append(30);
    });

    it('with set', () => {
       // When
       let result = testSubject.removeWithSet();

       // Then
        while(result != null) {
            expect(result.data).toBe(expectedUniqueLinkedList.data);
            result = result.next;
            expectedUniqueLinkedList = expectedUniqueLinkedList.next;
        }
    });

    it('without set', () => {
        // When
        let result = testSubject.remove();

        // Then
        while(result != null) {
            expect(result.data).toBe(expectedUniqueLinkedList.data);
            result = result.next;
            expectedUniqueLinkedList = expectedUniqueLinkedList.next;
        }
    });
});