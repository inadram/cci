export default class FindDuplicate {
    constructor(linkedList) {
        this.linkedList = linkedList;
    }

    //10,10,5
    removeWithSet() {
        let set = new Set();
        let current = this.linkedList;
        let prev = null;
        while (current != null) {
            if (set.has(current.data)) {
                prev.next = current.next;
            }
            set.add(current.data);
            prev = current;
            current = current.next;
        }
        return this.linkedList;
    }

    remove() {
        let current = this.linkedList;
        let next = current;
        while (current.next != null) {
            while (next.next != null) {
                if (next.next.data === current.data) {
                    next.next = next.next.next;
                } else {
                    next = next.next;
                }
            }
            current = current.next;
            next = current;
        }
        return this.linkedList;
    }

}