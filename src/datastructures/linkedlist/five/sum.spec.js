import Sum from './sum';
import LinkedList from '../lib/linkedlist';
describe('sum', () => {
    let testSubject;
    beforeEach(()=>{
        testSubject = new Sum();
    });

    it('sum two numbers forward', () => {
        // Given
        // 1 9 8 7
        // 8 7 6 5
        // 1 0 7 5 2
        let first = new LinkedList(1);
        first.append(9);
        first.append(8);
        first.append(7);

        let second = new LinkedList(8);
        second.append(7);
        second.append(6);
        second.append(5);

        let expected = new LinkedList(1);
        expected.append(0);
        expected.append(7);
        expected.append(5);
        expected.append(2);

        // when
        let result = testSubject.sumForward(first, second);

        // then
        while(result) {
            expect(result.data).toEqual(expected.data);
            result = result.next;
            expected = expected.next;
        }
    });

    it('sum two numbers backward', () => {
        // Given
        // 7 8 9 1
        // 5 6 7 8
        // 1 3 5 7 8
        let first = new LinkedList(1);
        first.append(9);
        first.append(8);
        first.append(7);

        let second = new LinkedList(8);
        second.append(7);
        second.append(6);
        second.append(5);

        let expected = new LinkedList(8);
        expected.append(7);
        expected.append(5);
        expected.append(3);
        expected.append(1);

        // when
        let result = testSubject.sumBackward(first, second);

        // then
        while(result) {
            expect(result.data).toEqual(expected.data);
            result = result.next;
            expected = expected.next;
        }
    })

    it('sum two numbers backward with different length', () => {
        // Given
        //   8 9 1
        // 5 6 7 8
        // 1 3 5 7 8
        let first = new LinkedList(1);
        first.append(9);
        first.append(8);

        let second = new LinkedList(8);
        second.append(7);
        second.append(6);
        second.append(5);

        let expected = new LinkedList(9);
        expected.append(6);
        expected.append(5);
        expected.append(6);

        // when
        let result = testSubject.sumBackwardWithDiffLength(first, second);

        // then
        while(result) {
            expect(result.data).toEqual(expected.data);
            result = result.next;
            expected = expected.next;
        }
    })
});