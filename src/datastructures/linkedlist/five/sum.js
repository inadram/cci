import LinkedList from '../lib/linkedlist';

const Sum = function() {
};

Sum.prototype.sumForward = function(first, second) {
    let firstSize = this.size(first);
    let secondSize = this.size(second);
    if (firstSize > secondSize) {
        second = this.prepend(second, firstSize - secondSize);
    } else {
        first = this.prepend(first, secondSize - firstSize);
    }

    let result = sum(first, second);
    if (result.ten > 0) {
        let node = new LinkedList(result.ten);
        node.next = result.result;
        result.result = node;
    }
    return result.result;

    function sum(node1, node2) {
        if (node1 === null) {
            return { 'ten': 0, result: null }
        }
        const total = sum(node1.next, node2.next);
        let sumValue = node1.data + node2.data + total.ten;
        const preNode = new LinkedList(sumValue % 10);
        preNode.next = total.result;
        total.result = preNode;
        total.ten = Math.floor(sumValue / 10);
        return total;
    }
};

Sum.prototype.sumBackward = function(first, second) {
    let firstSize = this.size(first);
    let secondSize = this.size(second);
    if (firstSize > secondSize) {
        second = this.append(second, secondSize - firstSize);
    } else {
        first = this.append(first, firstSize - secondSize);
    }
    return sum(first, second, 0);

    function sum(node1, node2, ten) {
        if (node1) {
            let sumValue = node1.data + node2.data + ten;
            let ones = sumValue % 10;
            let result = sum(node1.next, node2.next, Math.floor(sumValue / 10));
            let node = new LinkedList(ones);
            node.next = result;
            return node;
        } else {
            if (ten > 0) {
                return new LinkedList(ten);
            }
        }
    }
};

Sum.prototype.sumBackwardWithDiffLength = function(first, second) {
    const sum = (node1, node2, ten) => {
        if (node1 || node2) {
            let firstValue = ( node1 ) ? node1.data : 0;
            let secondValue = ( node2 ) ? node2.data : 0;
            node1 = (node1)? node1.next: node1;
            node2 = (node2)? node2.next: node2;
            let sumValue = firstValue + secondValue + ten;
            let node = new LinkedList(sumValue % 10);
            node.next = sum(node1, node2, Math.floor(sumValue / 10));
            return node;
        } else {
            if (ten > 10) {
                return new LinkedList(ten);
            }
        }
    };
    return sum(first, second, 0);

};

Sum.prototype.prepend = function(node, size) {
    while (size > 0) {
        let preNode = new LinkedList(0);
        preNode.next = node;
        node = preNode;
    }
    return node;
};

Sum.prototype.append = function(node, value) {
    while (node.next) {
        node = node.next;
    }
    node.next = new LinkedList(value);
};

Sum.prototype.size = function(node) {
    let count = 0;
    while (node) {
        count++;
        node = node.next;
    }
    return count;
};

export default Sum;