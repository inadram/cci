import LinkedList from '../lib/linkedlist';

export default class Partition {
    constructor(linkedList) {
        this.linkedList = linkedList;
    }

    partitionBy(value) {
        let smaller, bigger;
        while (this.linkedList) {
            const data = this.linkedList.data;
            if (data < value) {
                if (smaller) {
                    smaller.append(data);
                } else {
                    smaller = new LinkedList(data);
                }
            } else {
                if (bigger) {
                    bigger.append(data);
                } else {
                    bigger = new LinkedList(data);
                }
            }
            this.linkedList = this.linkedList.next;
        }
        let endOfSmaller = smaller;
        while (endOfSmaller.next) {
            endOfSmaller = endOfSmaller.next;
        }
        if (smaller) {
            endOfSmaller.next = bigger;
        } else {
            smaller = bigger;
        }
        return smaller;
    }

    partitionByNext(value) {
        let smaller, smallerStart, bigger, biggerStart;
        while (this.linkedList) {
            const data = this.linkedList.data;
            const node = new LinkedList(data);
            if (data < value) {
                if (!smaller) {
                    smallerStart = node;
                    smaller = smallerStart;
                } else {
                    smaller.next = node;
                    smaller = smaller.next;
                }
            } else {
                if (!bigger) {
                    biggerStart = node;
                    bigger = node;
                } else {
                    bigger.next = node;
                    bigger = bigger.next;
                }
            }
            this.linkedList = this.linkedList.next;
        }
        if (smallerStart) {
            smaller.next = biggerStart;
        } else {
            smallerStart = biggerStart;
        }

        return smallerStart;
    }

    partitionByNextAppend(value) {
        let smaller = new LinkedList();
        let bigger = new LinkedList();
        while (this.linkedList) {
            const data = this.linkedList.data;
            if (data < value) {
                Partition.append(smaller, data);
            } else {
                Partition.append(bigger, data);
            }
            this.linkedList = this.linkedList.next;
        }
        Partition.appendToTail(smaller, bigger.next);
        return smaller.next;
    }

    partitionByInPlace(value) {
        let size = Partition.size(this.linkedList);
        Partition.partitionInPlace(this.linkedList, size, value);
        return this.linkedList;
    }

    partitionByNode(value) {
        let node = this.linkedList;
        let smaller, bigger;
        while (node) {
            let next = node.next;
            if(node.data < value) {
                node.next = smaller;
                smaller = node;
            } else {
                node.next = bigger;
                bigger = node;
            }
            node = next
        }
        Partition.appendToTail(smaller, bigger);
        return smaller;
    }

    static partitionInPlace(node, size, value) {
        for (let i = 0; i < size -1 ; i++) {
            if (node.data >= value) {
                node.append(node.data);
                this.deleteNode(node);
            }else {
                node = node.next;
            }
        }
    }

    static size(node) {
        let count = 0;
        while (node) {
            count++;
            node = node.next;
        }
        return count;
    }

    static deleteNode(node) {
        node.data = node.next.data;
        node.next = node.next.next;
    }

    static append(node, data) {
        while (node.next) {
            node = node.next;
        }
        node.next = new LinkedList(data);
    }

    static appendToTail(smaller, bigger) {
        while (smaller.next) {
            smaller = smaller.next;
        }
        smaller.next = bigger;
    }
}