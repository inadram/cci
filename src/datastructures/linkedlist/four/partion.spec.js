import Partition from './partion';
import LinkedList from '../lib/linkedlist';

describe('partition', () => {
    let testSubject;
    beforeEach(() => {
        const linkedList = new LinkedList(10);
        linkedList.append(20);
        linkedList.append(15);
        linkedList.append(5);
        linkedList.append(50);
        linkedList.append(30);
        testSubject = new Partition(linkedList)
    });

    it('partition by 20', () => {
        // when
        const result = testSubject.partitionBy(20);

        // then
        expect(result.data).toBe(10);
        expect(result.next.data).toBe(15);
        expect(result.next.next.data).toBe(5);
        expect(result.next.next.next.data).toBe(20);
        expect(result.next.next.next.next.data).toBe(50);
        expect(result.next.next.next.next.next.data).toBe(30);
    });

    it('partition by next 20', () => {
        // when
        const result = testSubject.partitionByNext(20);

        // then
        expect(result.data).toBe(10);
        expect(result.next.data).toBe(15);
        expect(result.next.next.data).toBe(5);
        expect(result.next.next.next.data).toBe(20);
        expect(result.next.next.next.next.data).toBe(50);
        expect(result.next.next.next.next.next.data).toBe(30);
    });

    it('partition by next append 20', () => {
        // when
        const result = testSubject.partitionByNextAppend(20);

        // then
        expect(result.data).toBe(10);
        expect(result.next.data).toBe(15);
        expect(result.next.next.data).toBe(5);
        expect(result.next.next.next.data).toBe(20);
        expect(result.next.next.next.next.data).toBe(50);
        expect(result.next.next.next.next.next.data).toBe(30);
    });

    it('partition by inplace 20', () => {
        // when
        const result = testSubject.partitionByInPlace(20);

        // then
        expect(result.data).toBe(10);
        expect(result.next.data).toBe(15);
        expect(result.next.next.data).toBe(5);
        expect(result.next.next.next.data).toBe(30);
        expect(result.next.next.next.next.data).toBe(20);
        expect(result.next.next.next.next.next.data).toBe(50);
    })

    it('partition by node 20', () => {
        // when
        const result = testSubject.partitionByNode(20);

        // then
        expect(result.data).toBe(5);
        expect(result.next.data).toBe(15);
        expect(result.next.next.data).toBe(10);
        expect(result.next.next.next.data).toBe(30);
        expect(result.next.next.next.next.data).toBe(50);
        expect(result.next.next.next.next.next.data).toBe(20);
    })
});