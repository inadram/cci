export class Palindrome {
    constructor() {
    }

    isItRecHalf(node) {
        let size = Palindrome.size(node);
        return this.isPalindromeHalf(node, [], Math.floor(size / 2), size % 2 === 0);
    }

    // 1 2 3 3 2 1
    // 1 2 3 2 1
    isPalindromeHalf(node, stack, size, isEven) {
        if (!node) {
            return true;
        } else {
            if (size > 0) {
                stack.unshift(node.data);
            } else if (size === 0 && !isEven) {
                node = node.next
            }

            if (size <= 0 && stack.shift() !== node.data) {
                return false;
            }
            return this.isPalindromeHalf(node.next, stack, size - 1, isEven);
        }
    }

    isItRec(node) {
        return this.isPalindrome(node, []);
    }

    isPalindrome(node, queue) {
        if (!node) {
            return true;
        } else {
            queue.push(node.data);
            const result = this.isPalindrome(node.next, queue);
            return node.data === queue.shift() && result;
        }

    }

// 1 2 3 3 2 1
// 1 2 3 2 1
    isIt(node) {
        let nodeSize = Palindrome.size(node);
        let stack = [];
        for (let i = 0; i < Math.floor(nodeSize / 2); i++) {
            stack.unshift(node.data);
            node = node.next;
        }
        if (nodeSize % 2 !== 0) {
            node = node.next;
        }
        while (node) {
            if (node.data !== stack.shift()) {
                return false;
            }
            node = node.next;
        }
        return true;
    }

    static size(node) {
        let count = 0;
        while (node) {
            count++;
            node = node.next;
        }
        return count;
    }
}