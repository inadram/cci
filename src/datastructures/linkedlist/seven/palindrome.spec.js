import { Palindrome } from './palindrome';
import LinkedList from '../lib/linkedlist';

describe('palindrome', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new Palindrome();
    });
    describe('with loop', () => {
        it('true for palindrome with dup middle', () => {
            // Given
            const node = new LinkedList(1);
            node.append(2);
            node.append(3);
            node.append(3);
            node.append(2);
            node.append(1);
            // When
            const result = testSubject.isIt(node);

            // Then
            expect(result).toBeTruthy();
        });
        it('true for palindrome without dup middle', () => {
            // Given
            const node = new LinkedList(1);
            node.append(2);
            node.append(3);
            node.append(2);
            node.append(1);
            // When
            const result = testSubject.isIt(node);

            // Then
            expect(result).toBeTruthy();
        });
        it('false for non palindrome ', () => {
            // Given
            const node = new LinkedList(1);
            node.append(2);
            node.append(3);
            node.append(4);
            node.append(2);
            node.append(1);
            // When
            const result = testSubject.isIt(node);

            // Then
            expect(result).toBeFalsy();
        });
    });

    describe('with recursion', () => {
        it('true for palindrome with dup middle', () => {
            // Given
            const node = new LinkedList(1);
            node.append(2);
            node.append(3);
            node.append(3);
            node.append(2);
            node.append(1);
            // When
            const result = testSubject.isItRec(node);

            // Then
            expect(result).toBeTruthy();
        });
        it('true for palindrome without dup middle', () => {
            // Given
            const node = new LinkedList(1);
            node.append(2);
            node.append(3);
            node.append(2);
            node.append(1);
            // When
            const result = testSubject.isItRec(node);

            // Then
            expect(result).toBeTruthy();
        });
        it('false for non palindrome ', () => {
            // Given
            const node = new LinkedList(1);
            node.append(2);
            node.append(3);
            node.append(4);
            node.append(2);
            node.append(1);
            // When
            const result = testSubject.isItRec(node);

            // Then
            expect(result).toBeFalsy();
        });
    });

    describe('with half recursion', () => {
        it('true for palindrome with dup middle', () => {
            // Given
            const node = new LinkedList(1);
            node.append(2);
            node.append(3);
            node.append(3);
            node.append(2);
            node.append(1);
            // When
            const result = testSubject.isItRecHalf(node);

            // Then
            expect(result).toBeTruthy();
        });
        it('true for palindrome without dup middle', () => {
            // Given
            const node = new LinkedList(1);
            node.append(2);
            node.append(3);
            node.append(2);
            node.append(1);
            // When
            const result = testSubject.isItRecHalf(node);

            // Then
            expect(result).toBeTruthy();
        });
        it('false for non palindrome ', () => {
            // Given
            const node = new LinkedList(1);
            node.append(2);
            node.append(3);
            node.append(4);
            node.append(2);
            node.append(1);
            // When
            const result = testSubject.isItRecHalf(node);

            // Then
            expect(result).toBeFalsy();
        });
    });

});