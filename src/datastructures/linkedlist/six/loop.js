const Loop = function() {

    // 1 -> 2 -> 3 -> 4
    //      6  <----- 5
    //slow x
    //fast 2x
    //pre + in  = (2pre + 2in) - nxloop
    //pre+ in = loop
    // in+ remain = loop
    // pre = remain
    const find = (node) => {
        let start = node;
        let slow = node;
        let fast = node.next;
        while (slow !== fast) {
            slow = slow.next;
            fast = fast.next.next;
        }
        slow = slow.next;
        while (slow !== start) {
            slow = slow.next;
            start = start.next;
        }

        return start;

    };
    return {
        find: (node) => find(node)
    }
};

export default Loop;