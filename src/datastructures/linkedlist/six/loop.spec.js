import Loop from './loop';
import LinkedList from '../lib/linkedlist';

describe('loop', () => {
    let testSubject, node;
    beforeEach(() => {
        testSubject = new Loop();
        node = new LinkedList(1);
        node.append(2);
        node.append(3);
        node.append(4);
        node.append(5);
        node.append(6);
    });

    it('find beginning of the loop', () => {
        // Given
        node.next.next.next.next.next.next = node.next.next;

        // When
        const result = testSubject.find(node);

        // Then
        expect(result.data).toBe(node.next.next.data);
    });

    it('find beginning of the full loop', () => {
        // Given
        node.next.next.next.next.next.next = node;

        // When
        const result = testSubject.find(node);

        // Then
        expect(result.data).toBe(node.data);
    })
});