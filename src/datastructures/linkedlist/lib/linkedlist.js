export default class LinkedList {
    constructor(data) {
        this.data = data;
        this.next = null;
        this.prev = null;
        this.head = this;
        this.tail = null;
    }

    append(data) {
        let self = this;
        const node = new LinkedList(data);
        self.tail = node;
        while (self.next != null) {
            self = self.next;
        }
        node.prev = self;
        self.next = node;
    }

    remove() {
        let self = this;
        self.data = self.next.data;
        self.next = self.next.next;
        self.next.prev = self;
    }
}
