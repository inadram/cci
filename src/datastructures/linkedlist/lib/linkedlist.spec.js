import LinkedList from './linkedlist';

describe('linked list', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new LinkedList(10);
        testSubject.append(20);
        testSubject.append(30);
        testSubject.append(40);
    });
    it('append', () => {
        // When
        const result = testSubject.data;
        const result2 = testSubject.next.data;

        // Then
        expect(result).toBe(10);
        expect(result2).toBe(20);
    });

    it('head', () => {
        // When
        const result = testSubject.head.data;

        // Then
        expect(result).toBe(10);
    });

    it('tail', () => {
        // When
        const result = testSubject.tail.data;

        // Then
        expect(result).toBe(40);
    });

    it('remove', () => {
        // When
        testSubject.remove();
        const result = testSubject.head.data;
        const result2 = testSubject.next.data;
        const result3 = testSubject.next.next.data;
        const result4 = testSubject.tail.data;

        // Then
        expect(result).toBe(20);
        expect(result2).toBe(30);
        expect(result3).toBe(40);
        expect(result4).toBe(40);
    });

    it('prev', () => {
        // When
        const result = testSubject.prev;
        const result2 = testSubject.next.prev;
        const result3 = testSubject.next.next.prev;
        const result4 = testSubject.next.next.prev.prev;

        // Then
        expect(result).toBe(null);
        expect(result2.data).toBe(10);
        expect(result3.data).toBe(20);
        expect(result4.data).toBe(10);
    });

});