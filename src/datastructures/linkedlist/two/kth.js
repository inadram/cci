const Kth = (linkedList) => {

    const findBySize = (lastIndex) => {
        const size = getSize(linkedList);
        let index = ( size - lastIndex );
        while (index > 0) {
            linkedList = linkedList.next;
            index--;
        }
        return linkedList.data;
    };

    function findByRec(lastIndex) {
        return navigate(linkedList, lastIndex).data;
    }

    function navigate(node, index) {
        if (node === null) {
            return { data: null, index: 0 };
        }
        const result = navigate(node.next, index);
        if (result.index < index) {
            return { data: node.data, index: result.index + 1 };
        } else {
            return result;
        }
    }

    const getSize = (node) => {
        let size = 0;
        while (node != null) {
            size++;
            node = node.next;
        }
        return size;
    };


    return { findBySize: (index) => findBySize(index), findByRec: (lastIndex) => findByRec(lastIndex) }
};

export default Kth;