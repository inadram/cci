import Kth from './kth';
import LinkedList from '../lib/linkedlist';

describe('find kth element', () => {
    let testSubject;
    beforeEach(() => {
        const linkedList = new LinkedList(10);
        linkedList.append(20);
        linkedList.append(30);
        linkedList.append(40);
        testSubject = new Kth(linkedList);
    });

    it('find by size', () => {
        // when
        const result = testSubject.findBySize(2);

        // then
        expect(result).toBe(30);
    });

    it('find by rec', () => {
        // when
        const result = testSubject.findByRec(2);

        // then
        expect(result).toBe(30);
    });
});