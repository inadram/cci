const DeleteMiddle = function() {
};

DeleteMiddle.prototype.delete = function(linkedList) {
    if (linkedList === null || linkedList.next === null) {
        return null;
    }
    linkedList.data = linkedList.next.data;
    linkedList.next = linkedList.next.next;
    return linkedList;
};
DeleteMiddle.prototype.deleteByLoop = function(linkedList) {
    let node = linkedList;
    while (linkedList.next != null) {
        linkedList.data = linkedList.next.data;
        if (linkedList.next.next === null) {
            linkedList.next = null;
        } else {
            linkedList = linkedList.next;
        }
    }
    return node;
};

DeleteMiddle.prototype.deleteByRec = function(linkedList) {
    linkedList.data = linkedList.next.data;
    if (linkedList.next.next === null) {
        linkedList.next = null;
    } else {
        this.deleteByRec(linkedList.next);
    }
    return linkedList;
};

export default DeleteMiddle;