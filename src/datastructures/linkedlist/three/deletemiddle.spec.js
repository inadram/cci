import DeleteMiddle from './deletemiddle';
import LinkedList from '../lib/linkedlist';

describe('delete middle', () => {
    let testSubject, expectedNode, linkedList;
    beforeEach(() => {
        linkedList = new LinkedList(10);
        linkedList.append(20);
        linkedList.append(30);
        linkedList.append(40);
        expectedNode = new LinkedList(30);
        expectedNode.append(40);
        testSubject = new DeleteMiddle();
    });

    it('delete node by copy', () => {
        // when
        let result = testSubject.delete(linkedList.next);

        // then
        while (result != null) {
            expect(result.data).toBe(expectedNode.data);
            result = result.next;
            expectedNode = expectedNode.next;
        }
    });

    it('delete node with one size by copy', () => {
        // when
        let result = testSubject.delete(new LinkedList(10));

        // then
        expect(result).toBeNull();
    });

    it('delete node by loop', () => {
        // when
        let result = testSubject.deleteByLoop(linkedList.next);
        // then
        while (result != null) {
            expect(result.data).toBe(expectedNode.data);
            result = result.next;
            expectedNode = expectedNode.next;
        }
    });

    it('delete node by recursion', () => {
        // when
        let result = testSubject.deleteByRec(linkedList.next);
        // then
        while (result != null) {
            expect(result.data).toBe(expectedNode.data);
            result = result.next;
            expectedNode = expectedNode.next;
        }
    });
});