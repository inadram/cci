import LinkedList from '../../linkedlist/lib/linkedlist';

export class TreeDepth {
    constructor(tree) {
        this.tree = tree;
    }

    bfs() {
        let currentLevel = [];
        currentLevel.push(this.tree);
        let result = [];
        while (currentLevel.length > 0) {
            let childrenLevel = [];
            let partial;
            while (currentLevel.length > 0) {
                let current = currentLevel.shift();
                if (partial) {
                    partial.append(current.data);
                } else {
                    partial = new LinkedList(current.data)
                }
                if (current.left) {
                    childrenLevel.push(current.left);
                }
                if (current.right) {
                    childrenLevel.push(current.right);
                }
            }
            result.push(partial);
            currentLevel = childrenLevel;
        }
        return result;
    }

    array() {
        let queue = [ this.tree ];
        let values = [];
        while (queue.length > 0) {
            let current = queue.shift();
            values.push(( current ) ? current.data : 0);
            if (current) {
                queue.push(current.left);
                queue.push(current.right);
            }
        }

        function build(values, size) {
            if (size <= values.length) {
                let result = [];
                let node;
                for (let i = 0; i < size; i++) {
                    const value = values.shift();
                    if (value !== 0) {
                        if (node) {
                            node.append(value);
                        } else {
                            node = new LinkedList(value);
                        }
                    }
                }
                result.unshift(node);
                result = result.concat(build(values, size * 2));
                return result;
            }
        }

        return build(values, 1);
    }

    dfs() {
        function build(tree, level, result) {
            if(!tree){
                return result;
            } else {
                if (result.length <= level) {
                    result.push(new LinkedList(tree.data));
                } else {
                    result[ level ].append(tree.data);
                }
                build(tree.left, level + 1, result);
                return build(tree.right, level + 1, result);
            }
        }

        return build(this.tree, 0, []);
    }
}