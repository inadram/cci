import { TreeDepth } from './treedepth';
import LinkedList from '../../linkedlist/lib/linkedlist';
import { BinarySearchTree } from '../lib/binarysearchtree';

describe('depth of tree', () => {
    let testSubject;
    beforeEach(() => {
        /*
                             100
             50                         200
         25      75                 150     250
               65                110
         */
        const tree = new BinarySearchTree(100);
        tree.add(50);
        tree.add(75);
        tree.add(25);
        tree.add(65);
        tree.add(200);
        tree.add(150);
        tree.add(250);
        tree.add(110);

        testSubject = new TreeDepth(tree);
    });

    it('bfs linked list', () => {
        // when
        const result = testSubject.bfs();

        // then
        const one = result[ 0 ];
        expect(one.data).toBe(100);
        const two = result[ 1 ];
        expect(two.data).toBe(50);
        expect(two.next.data).toBe(200);
        const three = result[ 2 ];
        expect(three.data).toBe(25);
        expect(three.next.data).toBe(75);
        expect(three.next.next.data).toBe(150);
        expect(three.next.next.next.data).toBe(250);
        const four = result[ 3 ];
        expect(four.data).toBe(65);
        expect(four.next.data).toBe(110);
    });

    it('array linked list', () => {
        // when
        const result = testSubject.array();

        // then
        const one = result[ 0 ];
        expect(one.data).toBe(100);
        const two = result[ 1 ];
        expect(two.data).toBe(50);
        expect(two.next.data).toBe(200);
        const three = result[ 2 ];
        expect(three.data).toBe(25);
        expect(three.next.data).toBe(75);
        expect(three.next.next.data).toBe(150);
        expect(three.next.next.next.data).toBe(250);
        const four = result[ 3 ];
        expect(four.data).toBe(65);
        expect(four.next.data).toBe(110);
    });

    it('dfs linked list', () => {
        // when
        const result = testSubject.dfs();

        // then
        const one = result[ 0 ];
        expect(one.data).toBe(100);
        const two = result[ 1 ];
        expect(two.data).toBe(50);
        expect(two.next.data).toBe(200);
        const three = result[ 2 ];
        expect(three.data).toBe(25);
        expect(three.next.data).toBe(75);
        expect(three.next.next.data).toBe(150);
        expect(three.next.next.next.data).toBe(250);
        const four = result[ 3 ];
        expect(four.data).toBe(65);
        expect(four.next.data).toBe(110);
    });

});