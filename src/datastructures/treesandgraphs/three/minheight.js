import LinkedList from '../../linkedlist/lib/linkedlist';

const MinHeight = function() {
    this.build = (node) => {

        function buildWithDivide(node, left, right) {
            if (left < 0 || right >= node.length || left > right) {
                return null;
            }
            let mid = Math.floor(( left + right ) / 2);
            let tree = new LinkedList(node[ mid ]);
            tree.left = buildWithDivide(node, left, mid - 1);
            tree.right = buildWithDivide(node, mid + 1, right);
            return tree;
        }

        return buildWithDivide(node, 0, node.length - 1);
    }
};

export default MinHeight;