import MinHeight from './minheight';
import { BinarySearchTree } from '../lib/binarysearchtree';

describe('min height tree', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new MinHeight();
    });

    it('create min tree from ordered tree', () => {
        // given
        /*
        1 2 3 4 5 6 7 8 9 10
        0 1 2 3 4 5 6 7 8 9
                                     5
                      2                            8
                1        3                     6       9
                            4                     7      10

         */
        let input = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
        let expectedResult = new BinarySearchTree(5);
        expectedResult.left = new BinarySearchTree(2);
        expectedResult.left.left = new BinarySearchTree(1);
        expectedResult.left.right = new BinarySearchTree(3);
        expectedResult.left.right.left = new BinarySearchTree(4);

        expectedResult.right = new BinarySearchTree(8);
        expectedResult.right.right = new BinarySearchTree(9);
        expectedResult.right.right.right = new BinarySearchTree(10);
        expectedResult.right.left = new BinarySearchTree(6);
        expectedResult.right.left.right = new BinarySearchTree(7);
        // when
        let result = testSubject.build(input);

        // then
        while(result) {
            expect(result.data).toEqual(expectedResult.data);
            result = result.next;
            expectedResult = expectedResult.next;
        }

    });
});