export class PermutationOfBinarySearchTree {

    findPermutationOfArray(array) {
        let result = [ [] ];

        function createCombination(value, array) {
            let partialResult = [];
            for (let i = 0; i < array.length; i++) {
                let clonedArray = [ ...array[ i ] ];
                for (let j = 0; j < clonedArray.length; j++) {
                    let clonedInnerArray = [ ...clonedArray ];
                    clonedInnerArray.splice(j, 0, value);
                    partialResult.push(clonedInnerArray);
                }
                partialResult.push(clonedArray.concat(value));
            }
            return partialResult;
        }

        while (array.length > 0) {
            let pick = array.splice(0, 1)[ 0 ];
            result = createCombination(pick, result);
        }
        return result;
    }

    findPermutationOfArrayRec(array) {

        function rec(array, index) {
            let result = [];
            if (array.length - 1 <= index) {
                result.push([ array[ index ] ]);
                return result;
            } else {
                let partial = rec(array, index + 1);
                for (let i = 0; i < partial.length; i++) {
                    for (let j = 0; j < partial[ i ].length; j++) {
                        let cloned = [ ...partial[ i ] ];
                        cloned.splice(j, 0, array[ index ]);
                        result.push(cloned);
                    }
                    result.push(partial[ i ].concat(array[ index ]));
                }
                return result;
            }
        }

        return rec(array, 0);
    }

    findLeftAndRightSubtreeTree(tree) {
        function preOrder(tree, result = []) {
            if (!tree) {
                return result;
            }
            result.push(tree.data);
            preOrder(tree.left, result);
            preOrder(tree.right, result);
            return result;
        }

        return [ preOrder(tree.left), preOrder(tree.right) ]
    }

    findOrderedPermutation(prefix, array1, array2) {
        function generate(pre, arrayOne, arrayTwo) {
            let total = new Set();
            for(let one of arrayOne){
                for(let two of arrayTwo){
                    total.add([pre].concat(one).concat(two).toString());
                    for (let i = 0; i < one.length; i++) {
                        const array1Remain = one.slice(i + 1);
                        const array1Pick = one.slice(0, i + 1);
                        const array2Items = [ ...two ];
                        for (let j = 0; j < two.length; j++) {
                            let result = [];
                            result = result.concat(array2Items);
                            result = result.concat(array1Remain);
                            result.splice(j, 0, array1Pick);
                            result = result.reduce((result, value) => result.concat(value), []);
                            result.unshift(pre);
                            total.add(result.toString());
                        }
                    }
                }
            }
            return Array.from(total).map((value) => value.split(',').map(Number));
        }

        return generate(prefix, array1, array2).concat(generate(prefix, array2, array1));
    }

    findAllPossiblePath(tree) {
        if (!tree) {
            return [ [] ];
        }
        const prefix = tree.data;
        let left = this.findAllPossiblePath(tree.left);
        let right = this.findAllPossiblePath(tree.right);
        return this.findOrderedPermutation(prefix, left, right);
    }
}