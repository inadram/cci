import { PermutationOfBinarySearchTree } from './permutationofbinarysearchtree';
import { BinarySearchTree } from '../lib/binarysearchtree';

describe('permuation of binary search tree', () => {
	let testSubject;
	beforeEach(() => {
		testSubject = new PermutationOfBinarySearchTree();
	});

	describe('Permutation of array', () => {
		it('find for array size 1', () => {
			// given
			let array = [ 1 ];
			let expectedArray = [ [ 1 ] ];

			// when
			const result = testSubject.findPermutationOfArray(array);

			// then
			expect(result).toEqual(expectedArray);
		});
		it('find for array size 2', () => {
			// given
			let array = [ 1, 2 ];
			let expectedArray = [ [ 1, 2 ], [ 2, 1 ] ];

			// when
			const result = testSubject.findPermutationOfArray(array);

			// then
			expectedArray.forEach((array) => {
				expect(result.toString().includes(array.toString())).toBeTruthy();
			});
		});
		it('find for array size 3', () => {
			// given
			let array = [ 1, 2, 3 ];
			let expectedArray = [ [ 3, 1, 2 ], [ 1, 3, 2 ], [ 1, 2, 3 ], [ 3, 2, 1 ], [ 2, 3, 1 ], [ 2, 1, 3 ] ];

			// when
			const result = testSubject.findPermutationOfArray(array);

			// then
			expectedArray.forEach((array) => {
				expect(result.toString().includes(array.toString())).toBeTruthy();
			});
		});

		it('find for array size 5', () => {
			// given
			let array = [ 5, 2, 10, 7, 20 ];
			let expectedArray = [ [ 5, 10, 20, 7, 2 ], [ 5, 7, 20, 10, 2 ] ];

			// when
			const result = testSubject.findPermutationOfArray(array);

			// then
			expectedArray.forEach((array) => {
				expect(result.toString().includes(array.toString())).toBeTruthy();
			});
		});
	});

	describe('Permutationw of array rec', () => {
		it('find for array size 1', () => {
			// given
			let array = [ 1 ];
			let expectedArray = [ [ 1 ] ];

			// when
			const result = testSubject.findPermutationOfArrayRec(array);

			// then
			expect(result).toEqual(expectedArray);
		});
		it('find for array size 2', () => {
			// given
			let array = [ 1, 2 ];
			let expectedArray = [ [ 1, 2 ], [ 2, 1 ] ];

			// when
			const result = testSubject.findPermutationOfArrayRec(array);

			// then
			expectedArray.forEach((array) => {
				expect(result.toString().includes(array.toString())).toBeTruthy();
			});
		});
		it('find for array size 3', () => {
			// given
			let array = [ 1, 2, 3 ];
			let expectedArray = [ [ 3, 1, 2 ], [ 1, 3, 2 ], [ 1, 2, 3 ], [ 3, 2, 1 ], [ 2, 3, 1 ], [ 2, 1, 3 ] ];

			// when
			const result = testSubject.findPermutationOfArrayRec(array);

			// then
			expectedArray.forEach((array) => {
				expect(result.toString().includes(array.toString())).toBeTruthy();
			});
		});
	});

	describe('ordered permutation', () => {
		it('find array size 2', () => {
			// given
			let array1 = [ 1, 2 ];
			let array2 = [ 3, 4 ];
			let expectedArray = [
				[ 1, 3, 4, 2 ], [ 3, 1, 4, 2 ], [ 3, 4, 1, 2 ], [ 1, 2, 3, 4 ], [ 3, 1, 2, 4 ], [ 3, 4, 1, 2 ],
				[ 3, 1, 2, 4 ], [ 1, 3, 2, 4 ], [ 1, 2, 3, 4 ], [ 3, 4, 1, 2 ], [ 1, 3, 4, 2 ], [ 1, 2, 3, 4 ]
			];

			// when
			const result = testSubject.findOrderedPermutation(array1, array2);

			// then
			expectedArray.forEach((array) => {
				expect(result.toString().includes(array.toString())).toBeTruthy();
			});
		})
	});

	/*
					100
			 50           150
		  20    70      130   180
		10  25    80   120      200
	   2                            250
	 */
	describe('find possible path of creating bst', () => {
		let bst;
		beforeEach(() => {
			bst = new BinarySearchTree(100);
			bst.add(50);
			bst.add(150);
			bst.add(20);
			bst.add(70);
			bst.add(130);
			bst.add(180);
			bst.add(10);
			bst.add(25);
			bst.add(80);
			bst.add(120);
			bst.add(200);
			bst.add(2);
			bst.add(250);
		});

		it('find left and right subtree', () => {
			// Given
			let left = [ 50, 20, 10, 2, 25, 70, 80 ];
			let right = [ 150, 130, 120, 180, 200, 250 ];
			// when
			const result = testSubject.findLeftAndRightSubtreeTree(bst);

			//then
			expect(result[ 0 ]).toEqual(left);
			expect(result[ 1 ]).toEqual(right);
		});

		fit('find all possible subtree', () => {
			// given
			let expected = [ 100, 50, 20, 10, 2, 25, 70, 150, 130, 180, 200, 120, 80, 250 ];
			/*
						100
					50			150
				20		75
			 */
			// when
			const result = testSubject.findAllPossiblePath(bst);

			//then
			expect(result.map(value => value.toString()).includes(expected.toString())).toBeTruthy();
		});
	});
});
