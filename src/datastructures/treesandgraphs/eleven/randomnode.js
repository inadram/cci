export const RandomNode = function(value) {
    this.left = null;
    this.right = null;
    this.data = value;
    this.index = 0;
    this.depth = 1;
    this.insert = function(value) {
        const add = (value) => {
            let node = new RandomNode(value);
            this.index++;
            if (value < this.data) {
                if (!this.left) {
                    this.left = node;
                } else {
                    this.left.insert(value);
                }
            } else {
                if (!this.right) {
                    this.right = node;
                } else {
                    this.right.insert(value);
                }
            }
        };
        add(value);
        this.depth = this.getDepth(this);
    };

    this.getDepth = (tree) => {
        if (!tree) {
            return 0;
        }
        let left = this.getDepth(tree.left) + 1;
        let right = this.getDepth(tree.right) + 1;
        return Math.max(left, right);
    };

    this.del = function(value) {
        const find = (value, data, tree) => {
            if (data === value) {
                return null;
            } else if (value < data) {
                if (tree.left.data === value) {
                    let node = tree.left;
                    tree.left = null;
                    tree.index -= node.index + 1;
                    return node;
                } else {
                    let result = find(value, data, tree.left);
                    tree.index -= result.index + 1;
                    return result;
                }
            } else {
                if (tree.right.data === value) {
                    let node = tree.right;
                    tree.right = null;
                    return node;
                } else {
                    let result = find(value, data, tree.right);
                    tree.index -= result.index + 1;
                    tree.index -= node.index + 1;
                    return result;
                }
            }
        };
        const navigate = (tree) => {
            if (tree) {
                this.insert(tree.data);
                navigate(tree.left);
                navigate(tree.right);
            }
        };
        let node = find(value, this.data, this);
        navigate(node.left);
        navigate(node.right);
    };

    this.findRandom = function() {
        let randomNode = Math.floor(Math.random() * this.index) + 1;
        let tree = this;
        while (randomNode > 0) {
            if (tree.left && randomNode <= tree.left.index) {
                if (tree.right) {
                    randomNode -= tree.right.index;
                }
                tree = tree.left;
            } else {
                if (tree.left) {
                    randomNode -= tree.left.index;
                }
                tree = tree.right;
            }
            randomNode--;
        }
        return tree.data;
    };

    this.findRandomNode = function() {
        let index = Math.floor(Math.random() * this.index) + 1;

        function find(tree, index) {
            if (!tree) {
                return null;
            }
            if (index === 0) {
                return tree.data;
            }
            let data = find(tree.left, index - 1);
            if (data === null) {
                return find(tree.right, index - 1);
            }
        }

        return find(this, index);
    }
};