import { RandomNode } from './randomnode';

describe('find a random node', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new RandomNode(100);
    });
/*
        100, 1,2,3,4,5,6,7,8,9,10
 */
    it('find', () => {
        testSubject = new RandomNode(1);
        for (let i = 2; i < 11; i++) {
            testSubject.insert(i);
        }
        let result = 0;
        Array(100).fill(0).forEach(() => {
            result += testSubject.findRandom();
        });


        expect(result / 10 < 65).toBeTruthy();
        expect(result / 10 >= 55).toBeTruthy();
    });

    it('find random node', () => {
        testSubject = new RandomNode(1);
        for (let i = 2; i < 11; i++) {
            testSubject.insert(i);
        }
        let result = 0;
        Array(100).fill(0).forEach(() => {
            result += testSubject.findRandomNode();
        });


        expect(result / 10 < 65).toBeTruthy();
        expect(result / 10 >= 55).toBeTruthy();
    });
    /*
                100
            50          150
          20  70    130     250
            30                 300
     */
    it('insert with correct index', () => {
        // given
        testSubject.insert(50);
        testSubject.insert(150);
        testSubject.insert(20);
        testSubject.insert(70);
        testSubject.insert(130);
        testSubject.insert(250);
        testSubject.insert(300);

        //then
        expect(testSubject.data).toEqual(100);
        expect(testSubject.index).toEqual(7);
        expect(testSubject.left.data).toEqual(50);
        expect(testSubject.left.index).toEqual(2);
        expect(testSubject.right.data).toEqual(150);
        expect(testSubject.right.index).toEqual(3);
        expect(testSubject.left.left.data).toEqual(20);
        expect(testSubject.left.left.index).toEqual(0);
    });

    it('delete with correct position', () => {
        // given
        testSubject.insert(50);
        testSubject.insert(150);
        testSubject.insert(20);
        testSubject.insert(70);
        testSubject.insert(130);
        testSubject.insert(250);
        testSubject.insert(300);
        testSubject.insert(30);

        // when
       testSubject.del(50);

       // then
        expect(testSubject.data).toEqual(100);
        expect(testSubject.index).toEqual(7);
        expect(testSubject.left.data).toEqual(20);
        expect(testSubject.left.index).toEqual(2);
        expect(testSubject.left.right.data).toEqual(30);
        expect(testSubject.left.right.index).toEqual(1);
        expect(testSubject.left.right.right.data).toEqual(70);
        expect(testSubject.left.right.right.index).toEqual(0);
    });

    it('store correct depth', () => {
        // given
        testSubject.insert(50);
        testSubject.insert(150);
        testSubject.insert(20);
        testSubject.insert(70);
        testSubject.insert(130);
        testSubject.insert(250);
        testSubject.insert(300);
        testSubject.insert(30);

        // when

        // then
        expect(testSubject.depth).toEqual(4);
    });
});