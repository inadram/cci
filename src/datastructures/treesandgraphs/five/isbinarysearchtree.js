export const IsBinarySearchTree = () => {
    const check = (node) => {
        if (!node) {
            return true;
        }

        function isLessThan(node, data) {
            if (!node) {
                return true;
            }
            if (node.data > data) {
                return false;
            }
            return isLessThan(node.left, data) && isLessThan(node.right, data);
        }

        function isGreaterThan(node, data) {
            if (!node) {
                return true;
            }
            if (node.data < data) {
                return false;
            }
            return isGreaterThan(node.left, data) && isGreaterThan(node.right, data);
        }

        const status = isLessThan(node.left, node.data) && isGreaterThan(node.right, node.data);
        return status && check(node.left) && check(node.right);
    };

    function checkRec(node) {
        function checkWithMinMax(node, max, min) {
            if (!node) {
                return true;
            }
            if (node.data > max || node.data < min) {
                return false;
            }
            return checkWithMinMax(node.left, node.data, min) && checkWithMinMax(node.right, max, node.data);
        }

        return checkWithMinMax(node, Number.MAX_SAFE_INTEGER, Number.MIN_SAFE_INTEGER);
    }

    /*
                  100
           50             150
         25                   200
           35             175     250
   */
    function checkInorder(node) {
        function inorderNav(node, prev, isLeft) {
            if (!node) {
                return true;
            }
            const left = inorderNav(node.left, node.data, true);
            if (isLeft && prev < node.data) {
                return false;
            } else if (!isLeft && prev > node.data) {
                return false;
            }
            return left && inorderNav(node.right, node.data, false);
        }

        return inorderNav(node, Number.MAX_SAFE_INTEGER, true);
    }

    const checkInorderWithList = (node) => {
        function inorder(node) {
            let result = [];

            if (!node) {
                return result;
            }
            result = result.concat(inorder(node.left));
            result.push(node.data);
            return result.concat(inorder(node.right));
        }

        const list = inorder(node);
        for (let i = 0; i < list.length - 1; i++) {
            if (list[ i ] > list[ i + 1 ]) {
                return false;
            }
        }

        return true;
    };

    return {
        check: (node) => check(node),
        checkRec: (node) => checkRec(node),
        checkInorder: (node) => checkInorder(node),
        checkInorderWithList: (node) => checkInorderWithList(node)
    }
};