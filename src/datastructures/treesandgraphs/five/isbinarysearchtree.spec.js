import { IsBinarySearchTree } from './isbinarysearchtree';
import { BinarySearchTree } from '../lib/binarysearchtree';

describe('is binary search tree', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new IsBinarySearchTree();
    });
    /*
                    100
             50             150
           25                   200
             35             175     250
     */
    describe('normal check', () => {
        it('should return true for binary search tree', () => {
            // given
            let binarySearchTree = new BinarySearchTree(100);
            binarySearchTree.add(50);
            binarySearchTree.add(150);
            binarySearchTree.add(25);
            binarySearchTree.add(200);
            binarySearchTree.add(35);
            binarySearchTree.add(250);
            binarySearchTree.add(175);

            // when
            const result = testSubject.check(binarySearchTree);

            // then
            expect(result).toBe(true);
        });

        it('should return false for binary search tree', () => {
            // given
            let nonBinarySearchTree = new BinarySearchTree(100);
            nonBinarySearchTree.add(50);
            nonBinarySearchTree.add(150);
            nonBinarySearchTree.add(25);
            nonBinarySearchTree.add(200);
            nonBinarySearchTree.add(35);
            nonBinarySearchTree.add(250);
            nonBinarySearchTree.add(175);
            nonBinarySearchTree.left.left.right.data = 60;

            // when
            const result = testSubject.check(nonBinarySearchTree);

            // then
            expect(result).toBe(false);
        });
    });

    describe('recursive check', () => {
        it('should return true for binary search tree', () => {
            // given
            let binarySearchTree = new BinarySearchTree(100);
            binarySearchTree.add(50);
            binarySearchTree.add(150);
            binarySearchTree.add(25);
            binarySearchTree.add(200);
            binarySearchTree.add(35);
            binarySearchTree.add(250);
            binarySearchTree.add(175);

            // when
            const result = testSubject.checkRec(binarySearchTree);

            // then
            expect(result).toBe(true);
        });

        it('should return false for binary search tree', () => {
            // given
            let nonBinarySearchTree = new BinarySearchTree(100);
            nonBinarySearchTree.add(50);
            nonBinarySearchTree.add(150);
            nonBinarySearchTree.add(25);
            nonBinarySearchTree.add(200);
            nonBinarySearchTree.add(35);
            nonBinarySearchTree.add(250);
            nonBinarySearchTree.add(175);
            nonBinarySearchTree.left.left.right.data = 60;

            // when
            const result = testSubject.checkRec(nonBinarySearchTree);

            // then
            expect(result).toBe(false);
        });
    });

    describe('inorder check', () => {
        it('should return true for binary search tree', () => {
            // given
            let binarySearchTree = new BinarySearchTree(100);
            binarySearchTree.add(50);
            binarySearchTree.add(150);
            binarySearchTree.add(25);
            binarySearchTree.add(200);
            binarySearchTree.add(35);
            binarySearchTree.add(250);
            binarySearchTree.add(175);

            // when
            const result = testSubject.checkInorder(binarySearchTree);

            // then
            expect(result).toBe(true);
        });

        it('should return false for binary search tree', () => {
            // given
            let nonBinarySearchTree = new BinarySearchTree(100);
            nonBinarySearchTree.add(50);
            nonBinarySearchTree.add(150);
            nonBinarySearchTree.add(25);
            nonBinarySearchTree.add(200);
            nonBinarySearchTree.add(35);
            nonBinarySearchTree.add(250);
            nonBinarySearchTree.add(175);
            nonBinarySearchTree.left.left.right.data = 15;

            // when
            const result = testSubject.checkInorder(nonBinarySearchTree);

            // then
            expect(result).toBe(false);
        });
    });

    describe('inorder check with list', () => {
        it('should return true for binary search tree', () => {
            // given
            let binarySearchTree = new BinarySearchTree(100);
            binarySearchTree.add(50);
            binarySearchTree.add(150);
            binarySearchTree.add(25);
            binarySearchTree.add(200);
            binarySearchTree.add(35);
            binarySearchTree.add(250);
            binarySearchTree.add(175);

            // when
            const result = testSubject.checkInorderWithList(binarySearchTree);

            // then
            expect(result).toBe(true);
        });

        it('should return false for binary search tree', () => {
            // given
            let nonBinarySearchTree = new BinarySearchTree(100);
            nonBinarySearchTree.add(50);
            nonBinarySearchTree.add(150);
            nonBinarySearchTree.add(25);
            nonBinarySearchTree.add(200);
            nonBinarySearchTree.add(35);
            nonBinarySearchTree.add(250);
            nonBinarySearchTree.add(175);
            nonBinarySearchTree.left.left.right.data = 60;

            // when
            const result = testSubject.checkInorderWithList(nonBinarySearchTree);

            // then
            expect(result).toBe(false);
        });
    });
});