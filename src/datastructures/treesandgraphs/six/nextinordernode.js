export const NextInorderNode = function() {
    this.next = function(node) {
        if (node.right) {
            return findLeftMost(node.right);
        } else {
            return findParent(node.parent, node.data);
        }
    };

    function findParent(parent, data) {
        if (parent.data > data) {
            return parent;
        } else {
            return findParent(parent.parent, data);
        }
    }

    function findLeftMost(node) {
        if (!node.left) {
            return node;
        }
        return findLeftMost(node.left);
    }
};