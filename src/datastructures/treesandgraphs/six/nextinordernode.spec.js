import { NextInorderNode } from './nextinordernode';
import { BinarySearchTree } from '../lib/binarysearchtree';

describe('next inorder node', () => {
    let testSubject, tree;
    beforeEach(() => {
        testSubject = new NextInorderNode();
        tree = new BinarySearchTree(100);
        tree.add(50);
        tree.add(150);
        tree.add(25);
        tree.add(35);
        tree.add(110);
        tree.add(200);
        tree.add(250);
        tree.add(11);
        tree.add(1);
    });
    // given
    /*
               100
       50                150
     25              110     200
 11      35                      250
1
     */

    it('find next node when right have no left', () => {
        // when
        const result = testSubject.next(tree.left.left);

        // then
        expect(result.data).toEqual(35);
    });

    it('find next node when right have left ', () => {
        // when
        const result = testSubject.next(tree);

        // then
        expect(result.data).toEqual(110);
    });

    it('find next node when no right ', () => {
        // when
        const result = testSubject.next(tree.left.left.right);

        // then
        expect(result.data).toEqual(50);
    });

    it('find next node when no left ', () => {
        // when
        const result = testSubject.next(tree.left.left.left.left);

        // then
        expect(result.data).toEqual(11);
    });
});