export default class FindRoute {
    constructor(graph) {
        this.graph = graph;
    }

    checkWithBfs(source, destination) {
        let queue = [];
        let visited = [];
        const sourceNode = this.graph.nodes.get(source);
        queue.push(sourceNode);
        while (queue.length > 0) {
            let node = queue.shift();
            if (node.data === destination) {
                return true;
            } else if (!visited.includes(node.data)) {
                visited.push(node.data);
                node.outs.forEach(( value => queue.push(value) ));
            }
        }
        return false;
    }

    checkWithDfsForward(source, destination, visited = [], status = false) {
        let outs = this.graph.nodes.get(source).outs;
        for (let [ value ] of outs) {
            if (value === destination) {
                status = true;
            }
            if (!visited.includes(value) && !status) {
                visited.push(value);
                status = this.checkWithDfsForward(value, destination, visited, status);
            }
        }
        return status;
    }

    checkWithDfsReturn(source, destination, visited = []) {
        let sourceNode = this.graph.nodes.get(source);
        let result = false;
        for (let node of sourceNode.outs.keys()) {
            if (node === destination) {
                return true;
            }
            if (!visited.includes(source) && !result) {
                visited.push(node);
                result = this.checkWithDfsReturn(node, destination, visited);
            }
        }
        return result;
    }

    checkWithDfsIterator(source, destination, visited = []) {
        let edges = this.graph.nodes.get(source).outs;
        let getEdges = (next) => {
            return this.graph.nodes.get(next).outs;
        };
        return checkWithIterator(edges, destination, visited);
        function checkWithIterator(edges, destination, visted) {
            const next = edges.keys().next();
            edges.delete(next.value);
            let found = false;
            if(!next.done && !found) {
                if(next.value === destination) {
                    return true;
                } else if(!visted.includes(next.value)) {
                    visted.push(next.value);
                    found = checkWithIterator(getEdges(next.value), destination, visted)
                }
                found = found || checkWithIterator(edges, destination, visted);
            }
            return found;
        }
    }


}