import FindRoute from './findroute';
import { DirectedGraph } from '../lib/directedgraph';

describe('find route', () => {
    let testSubject;
    /* a-> b -> f
        -> c -> d -> e
    */
    beforeEach(() => {
        let graph = new DirectedGraph();
        graph.addVertex('a');
        graph.addVertex('b');
        graph.addVertex('c');
        graph.addVertex('d');
        graph.addVertex('e');
        graph.addVertex('f');
        graph.addEdge('a', 'b');
        graph.addEdge('a', 'c');
        graph.addEdge('c', 'd');
        graph.addEdge('d', 'e');
        graph.addEdge('b', 'f');
        testSubject = new FindRoute(graph);
    });
    describe('with bfs', () => {
        it('return true when there is a route between two nodes', () => {
            // given
            const result = testSubject.checkWithBfs('a', 'd');
            // then
            expect(result).toBe(true);
        });

        it('return false when there is no route between two nodes', () => {
            // given
            const result = testSubject.checkWithBfs('f', 'd');
            // then
            expect(result).toBe(false);
        })
    });

    describe('with dfs forward', () => {
        it('return true when there is a route between two nodes', () => {
            // given
            const result = testSubject.checkWithDfsForward('a', 'd');
            // then
            expect(result).toBe(true);
        });

        it('return false when there is no route between two nodes', () => {
            // given
            const result = testSubject.checkWithDfsForward('f', 'd');
            // then
            expect(result).toBe(false);
        })
    });

    describe('with dfs return', () => {
        it('return true when there is a route between two nodes', () => {
            // given
            const result = testSubject.checkWithDfsReturn('a', 'd');
            // then
            expect(result).toBe(true);
        });

        it('return false when there is no route between two nodes', () => {
            // given
            const result = testSubject.checkWithDfsReturn('f', 'd');
            // then
            expect(result).toBe(false);
        })
    });

    describe('with dfs iterator', () => {
        it('return true when there is a route between two nodes', () => {
            // given
            const result = testSubject.checkWithDfsIterator('a', 'd');
            // then
            expect(result).toBe(true);
        });

        it('return false when there is no route between two nodes', () => {
            // given
            const result = testSubject.checkWithDfsIterator('f', 'd');
            // then
            expect(result).toBe(false);
        })
    });
});