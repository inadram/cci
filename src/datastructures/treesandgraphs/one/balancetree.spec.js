import BalanceTree from './balancetree';
import { BinarySearchTree } from '../lib/binarysearchtree';

describe('is balance', () => {
    let testSubject, balanceTree, unBalanceTree;
    beforeEach(() => {
        testSubject = new BalanceTree();
        /*
                    100
            50                150
          20  70      120             180
                          130
       */

        balanceTree = new BinarySearchTree(100);
        balanceTree.add(50);
        balanceTree.add(150);
        balanceTree.add(20);
        balanceTree.add(70);
        balanceTree.add(70);
        balanceTree.add(120);
        balanceTree.add(180);
        balanceTree.add(130);
        /*
                     100
            50                150
          20  70      120             180
                        130
                          140
        */
        unBalanceTree = new BinarySearchTree(100);
        unBalanceTree.add(50);
        unBalanceTree.add(150);
        unBalanceTree.add(20);
        unBalanceTree.add(70);
        unBalanceTree.add(70);
        unBalanceTree.add(120);
        unBalanceTree.add(180);
        unBalanceTree.add(130);
        unBalanceTree.add(140);
    });

    describe('recursion', ()=> {
        it('true for balance tree', () => {
            // when
            const result = testSubject.check(balanceTree);

            // then
            expect(result).toBe(true);

        });

        it('false for balance tree', () => {
            // when
            const result = testSubject.check(unBalanceTree);

            // then
            expect(result).toBe(false);

        });
    });

    describe('branch recursion', ()=> {
        it('true for balance tree', () => {
            // when
            const result = testSubject.checkWithBranch(balanceTree);

            // then
            expect(result).toBe(true);

        });

        it('false for balance tree', () => {
            // when
            const result = testSubject.checkWithBranch(unBalanceTree);

            // then
            expect(result).toBe(false);

        });
    })

});