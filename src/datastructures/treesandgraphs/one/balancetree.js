const BalanceTree = function() {
    function maxHeight(node) {
        if (!node) {
            return 0;
        }
        let left = maxHeight(node.left) + 1;
        let right = maxHeight(node.right) + 1;
        return Math.max(left, right);
    }

    function minHeight(node) {
        if (!node) {
            return 0;
        }
        let left = minHeight(node.left) + 1;
        let right = minHeight(node.right) + 1;
        return Math.min(left, right);
    }

    const check = (node) => {
        return maxHeight(node) - minHeight(node) < 2;
    };

    const branchCheck = (node) => {
        if(!node){
            return true;
        }
        if(maxHeight(node.left) - maxHeight(node.right)>1) {
            return false;
        }
        return branchCheck(node.left) &&  branchCheck(node.right);
    };

    return {
        check: (node) => check(node),
        checkWithBranch: (node) => branchCheck(node)
    }
};

export default BalanceTree;