import { DirectedGraph } from '../lib/directedgraph';

export function Dependent() {
    function find(projct, dependencies) {
        let queue = projct;
        let result = [];
        while (queue.length > 0) {
            let current = queue.shift();
            for (let dependent of dependencies) {
                if (dependent[ current ] && !result.includes(dependent[ current ])) {
                    result.push(dependent[ current ]);
                }
            }
            if (!result.includes(current)) {
                result.push(current);
            }
        }
        return result;
    }

    function findByGraph(project, dependencies) {
        function buildGraph(project, dependencies) {
            let directedGraph = new DirectedGraph();
            project.forEach((value) => directedGraph.addVertex(value));
            dependencies.forEach((dependent) => directedGraph.addEdge(dependent[ 0 ], dependent[ 1 ]));
            return directedGraph;
        }

        let graph = buildGraph(project, dependencies);

        function findDependencies(nodes, result) {
            for (let [ key, value ] of nodes) {
                for (let [ k, v ] of value.outs) {
                    findDependencies(v.outs, result);
                    result.add(k);
                }
                result.add(key);
            }
            return result;
        }

        return Array.from(findDependencies(graph.nodes, new Set()));
    }

    function findByGraphBFS(project, dependencies) {
        function buildGraph(project, dependencies) {
            let directedGraph = new DirectedGraph();
            project.forEach((value) => directedGraph.addVertex(value));
            dependencies.forEach((dependent) => directedGraph.addEdge(dependent[ 0 ], dependent[ 1 ]));
            return directedGraph;
        }

        let graph = buildGraph(project, dependencies);
        let result = [];
        for(let [key, node] of graph.nodes){
            let queue = [node];
            while(queue.length){
                let current = queue[0];
                if(!current.outs.size){
                    result.push(current.data);
                    queue.shift();
                } else {
                    Array.from(current.outs.values()).concat(result)
                }
            }
        }
    }

    return { find, findByGraph, findByGraphBFS }
}