import { Dependent } from './dependent';

describe('find dependecies', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new Dependent();
    });

    it('print in correct order', () => {
        // Given
        const project = [ 'a', 'b', 'c', 'd', 'e', 'f' ];
        const dependencies = [ { 'a': 'f' }, { 'd': 'a' }, { 'b': 'f' }, { 'd': 'b' }, { 'c': 'd' } ];
        const expectedResult = [ 'f', 'a', 'b', 'd', 'c', 'e' ];
        // when
        const result = testSubject.find(project, dependencies);

        //then
        expect(result).toEqual(expectedResult);
    });

    it('print with graph', () => {
        // Given
        const project = [ 'a', 'b', 'c', 'd', 'e', 'f' ];
        const dependencies = [ [ 'a', 'f' ], [ 'd', 'a' ], [ 'b', 'f' ], [ 'd', 'b' ], [ 'c', 'd' ] ];
        const expectedResult = [ 'f', 'a', 'b', 'd', 'c', 'e' ];
        // when
        const result = testSubject.findByGraph(project, dependencies);

        //then
        expect(result).toEqual(expectedResult);
    });

    it('print with graph bfs', () => {
        // Given
        const project = [ 'a', 'b', 'c', 'd', 'e', 'f' ];
        const dependencies = [ [ 'a', 'f' ], [ 'd', 'a' ], [ 'b', 'f' ], [ 'd', 'b' ], [ 'c', 'd' ] ];
        const expectedResult = [ 'f', 'a', 'b', 'd', 'c', 'e' ];
        // when
        const result = testSubject.findByGraphBFS(project, dependencies);

        //then
        expect(result).toEqual(expectedResult);
    });
});