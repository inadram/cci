import { SumPath } from './sumpath';
import { BinaryTree } from '../lib/binarytree';

describe('sum path', () => {
    let testSubject, binaryTree;
    /*
                    1
               2           3
            4   3        6   1
         -3  1    1
     */
    beforeEach(() => {
        binaryTree = new BinaryTree(1);
        binaryTree.add(2);
        binaryTree.add(3);
        binaryTree.add(4);
        binaryTree.add(3);
        binaryTree.add(6);
        binaryTree.add(1);
        binaryTree.add(-3);
        binaryTree.add(1);
        binaryTree.add(1);
        testSubject = new SumPath(binaryTree);
    });

    describe('by navigate', () => {
        it('find all path with 7', () => {
            // given
            const expectedPath = [ [ 1, 2, 3, 1 ], [ 1, 2, 4 ], [ 2, 4, 1 ] ];

            // when
            const result = testSubject.findAll(7);
            // then
            expectedPath.forEach(value => expect(result.toString().includes(value.toString())).toBeTruthy());
        })
    });

    describe('by recursive', () => {
        it('find all path with 7', () => {
            // given
            const expectedPath = 3;

            // when
            const result = testSubject.findAllByRec(7);
            // then
            expect(result).toEqual(expectedPath);
        })
    });

    describe('by running sum', () => {
        it('find all path with 7', () => {
            // given
            const expectedPath = 3;

            // when
            const result = testSubject.findAllByRunningSum(7);
            // then
            expect(result).toEqual(expectedPath);
        })
    });
});