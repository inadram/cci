export function SumPath(tree) {

    const findAll = (value) => {
        return navigate(tree);

        function navigate(tree) {
            let result = [];
            if (!tree) {
                return result;
            }
            else if (tree) {
                result = result.concat(find(value, tree, [], []));
                result = result.concat(navigate(tree.left));
                return result.concat(navigate(tree.right));
            }
        }

        function find(value, tree, partialSum, result) {
            if (!tree) {
                return result;
            }
            else if (tree) {
                partialSum.push(tree.data);
                const partialResult = partialSum.reduce((a, b) => a + b, 0);
                if (partialResult === value) {
                    result.push([ ...partialSum ]);
                }
                find(value, tree.left, partialSum, result);
                find(value, tree.right, partialSum, result);
                partialSum.pop();
                return result;
            }
        }
    };

    const findAllByRec = (value) => {

        return preOrder(tree, value, 0, []);

        function preOrder(tree, value, level, values) {
            let result = 0;
            if (!tree) {
                return result;
            }
            ( values.length <= level ) ? values.push(tree.data) : values[ level ] = tree.data;
            if (values.reduce((a, b) => a + b, 0) === value) {
                result += 1;
            }
            result += preOrder(tree.left, value, level + 1, values);
            result += preOrder(tree.right, value, level + 1, values);
            return result;
        }

    };

    // 1, 3, 4 , 2 , -2 , 3
    //1   4  8   10  8  11
    const findAllByRunningSum = (value) => {
        const map = new Map();
        map.set(0, 1);
        return runningSum(tree, value, 0, map);

        function runningSum(tree, value, sum, paths) {
            let result = 0;
            if (!tree) {
                return result;
            }

            sum += tree.data;
            if (!paths.has(sum)) {
                paths.set(sum, 0);
            }
            paths.set(sum, paths.get(sum) + 1);
            if (paths.has(sum - value)) {
                result += paths.get(sum - value);
            }
            result += runningSum(tree.left, value, sum, paths);
            result += runningSum(tree.right, value, sum, paths);
            paths.set(sum, paths.get(sum) - 1);
            return result;
        }
    };

    return {
        findAll: (value) => findAll(value),
        findAllByRec: (value) => findAllByRec(value),
        findAllByRunningSum: (value) => findAllByRunningSum(value)
    }
}