import { BinarySearchTree } from '../lib/binarysearchtree';
import { CommonAncestor } from './commonancestor';
import { BinaryTree } from '../lib/binarytree';

describe('common ancestor on a binary search tree', () => {
    let testSubject, binarySearchTree, binaryTree;
    /*
                100
            50      200
          25  75   150  250
     */
    describe('binary search tree', () => {
        beforeEach(() => {
            binarySearchTree = new BinarySearchTree(100);
            binarySearchTree.add(50);
            binarySearchTree.add(200);
            binarySearchTree.add(25);
            binarySearchTree.add(75);
            binarySearchTree.add(150);
            binarySearchTree.add(250);
            testSubject = new CommonAncestor(binarySearchTree);
        });

        it('find common when left and right', () => {
            // when
            const result = testSubject.find(binarySearchTree.left.left, binarySearchTree.left.right);

            // then
            expect(result).toBe(binarySearchTree.left);
        });

        it('find common when both left', () => {
            // when
            const result = testSubject.find(binarySearchTree.left.left, binarySearchTree.left);

            // then
            expect(result).toBe(binarySearchTree);
        });
    });

    /*
                100
           110      120
        130  140   150  160
   */
    describe('non binary search tree', () => {
        beforeEach(() => {
            binaryTree = new BinaryTree(100);
            binaryTree.add(110);
            binaryTree.add(120);
            binaryTree.add(130);
            binaryTree.add(140);
            binaryTree.add(150);
            binaryTree.add(160);
            testSubject = new CommonAncestor(binaryTree);
        });

        it('find common when left and right', () => {
            // when
            const result = testSubject.findByNavigate(binaryTree.left.left, binaryTree.left.right);

            // then
            expect(result).toBe(binaryTree.left);
        });

        it('find common when both left', () => {
            // when
            const result = testSubject.findByNavigate(binaryTree.left.left, binaryTree.left);

            // then
            expect(result).toBe(binaryTree);
        });
    });

    describe('non binary search tree with parent', () => {
        beforeEach(() => {
            binaryTree = new BinaryTree(100);
            binaryTree.add(110);
            binaryTree.add(120);
            binaryTree.add(130);
            binaryTree.add(140);
            binaryTree.add(150);
            binaryTree.add(160);
            testSubject = new CommonAncestor(binaryTree);
        });

        it('find common when left and right', () => {
            // when
            const result = testSubject.findByParent(binaryTree.left.left, binaryTree.left.right);

            // then
            expect(result.data).toBe(binaryTree.left.data);
        });

        it('find common when both left', () => {
            // when
            const result = testSubject.findByParent(binaryTree.left.left, binaryTree.left);

            // then
            expect(result.data).toBe(binaryTree.data);
        });
    });

    describe('non binary search tree with parent and sibling', () => {
        beforeEach(() => {
            binaryTree = new BinaryTree(100);
            binaryTree.add(110);
            binaryTree.add(120);
            binaryTree.add(130);
            binaryTree.add(140);
            binaryTree.add(150);
            binaryTree.add(160);
            testSubject = new CommonAncestor(binaryTree);
        });

        it('find common when left and right', () => {
            // when
            const result = testSubject.findByParentCoverSibling(binaryTree.left.left, binaryTree.left.right);

            // then
            expect(result.data).toBe(binaryTree.left.data);
        });

        it('find common when both left', () => {
            // when
            const result = testSubject.findByParentCoverSibling(binaryTree.left.left, binaryTree.left);

            // then
            expect(result.data).toBe(binaryTree.data);
        });
    });

});
