export const CommonAncestor = (tree) => {
    function find(node1, node2) {
        return findCommon(tree, node1, node2);

        function findCommon(tree, node1, node2) {
            if (!tree) {
                return tree;
            } else {
                if (node1.data < tree.data && node2.data < tree.data) {
                    tree = tree.left;
                } else if (node1.data > tree.data && node2.data > tree.data) {
                    tree = tree.right;
                } else if (node1 === tree || node2 === tree) {
                    return tree.parent;
                } else {
                    return tree;
                }
                return findCommon(tree, node1, node2);
            }
        }
    }

    const findByNavigate = (node1, node2) => {
        function contain(tree, node) {
            if (!tree) {
                return false;
            }
            if (tree === node) {
                return true;
            }
            return contain(tree.left, node) || contain(tree.right, node);
        }

        function findCommon(tree, node1, node2) {
            if (!tree) {
                return null;
            } else {
                if (tree === node1) {
                    return { common: null };
                }
                let result = findCommon(tree.left, node1, node2) || findCommon(tree.right, node1, node2);
                if (!result.common) {
                    if (contain(tree, node2) && tree !== node2) {
                        result.common = tree;
                    }
                }
                return result;
            }
        }

        return findCommon(tree, node1, node2).common;
    };

    const findByParent = (node1, node2) => {
        function inParent(node, parent) {
            if (!parent) {
                return false;
            }
            if (node.data === parent.data) {
                return true;
            }
            return inParent(node, parent.parent)
        }

        function isChild(node, child) {
            if (!node) {
                return false;
            }
            if (node.data === child.data) {
                return true;
            }
            return isChild(node.left, child) || isChild(node.right, child);
        }

        if (isChild(node1, node2)) {
            return node1.parent;
        } else if (isChild(node2, node1)) {
            return node2.parent;
        }
        while (!inParent(node1, node2)) {
            node1 = node1.parent;
        }
        return node1;
    };

    const findByParentCoverSibling = (node1, node2) => {
        if(isCover(node1, node2)){
            return node1.parent;
        } else if (isCover(node2, node1)){
            return node2.parent;
        }

       function isCover(parent, node) {
           if(!parent) {
               return false;
           }
           if(parent.data === node.data) {
               return true;
           }
           return isCover(parent.left, node) || isCover(parent.right, node);
       }
       function getSibling(node) {
           if(!node || !node.parent) {
               return null;
           }
           const parent = node.parent;
           if(parent.left.data === node.data) {
               return parent.right;
           } else {
               return parent.left;
           }
       }
       let sibling = getSibling(node1);
       let parent = node1.parent;
       while (!isCover(sibling, node2)){
           sibling = getSibling(parent);
           parent = parent.parent;
       }
       return parent;
    };

    return {
        find: (node1, node2) => find(node1, node2),
        findByNavigate: (node1, node2) => findByNavigate(node1, node2),
        findByParent: (node1, node2) => findByParent(node1, node2),
        findByParentCoverSibling: (node1, node2) => findByParentCoverSibling(node1, node2)
    }
};