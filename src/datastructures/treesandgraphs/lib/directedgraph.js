export const DirectedGraph = function() {
    this.nodes = new Map();
    this.addVertex = function(data) {
        if (!this.nodes.has(data)) {
            this.nodes.set(data, new GraphNode(data));
        }
    };

    this.removeVertex = (nodeName) => {
        this.nodes.get(nodeName).outs.forEach((value) => {
            this.nodes.get(value.data).ins.delete(nodeName);
        });
        this.nodes.delete(nodeName);
    };

    this.addEdge = function(source, destination) {
        const sourceNode = this.nodes.get(source);
        const destinationNode = this.nodes.get(destination);
        sourceNode.to(destinationNode);
        destinationNode.from(sourceNode);
    };

    this.removeEdge = (source, destination) => {
        const sourceNode = this.nodes.get(source);
        const destinationNode = this.nodes.get(destination);
        destinationNode.ins.delete(source);
        sourceNode.outs.delete(destination);
    }
};

export const GraphNode = function(data) {
    this.data = data;
    this.ins = new Map();
    this.outs = new Map();

    this.from = (node) => {
        this.ins.set(node.data, node);
    };

    this.to = (node) => {
        this.outs.set(node.data, node);
    };
};