import { BinarySearchTree } from './binarysearchtree';

describe('binarysearchtree', () => {
    let testSubject;
    beforeEach(() => {
        /*
                     100
               50            150
            40    70     130
         30
         */
        testSubject = new BinarySearchTree(100);
        testSubject.add(50);
        testSubject.add(70);
        testSubject.add(40);
        testSubject.add(30);
        testSubject.add(150);
        testSubject.add(130);
    });

    it('should be binary search tree', () => {
        // then
        expect(testSubject.data).toBe(100);
        expect(testSubject.left.data).toBe(50);
        expect(testSubject.left.left.data).toBe(40);
        expect(testSubject.left.left.left.data).toBe(30);
        expect(testSubject.left.right.data).toBe(70);
        expect(testSubject.right.data).toBe(150);
        expect(testSubject.right.left.data).toBe(130);
    });

    it('should return correct parent', () => {
        //then
        expect(testSubject.right.parent.data).toBe(100);
        expect(testSubject.right.left.parent.data).toBe(150);
        expect(testSubject.right.left.parent.parent.data).toBe(100);
    });
});