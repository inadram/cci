export class TreeNavigation {
    constructor(){
        this.result = [];
    }

    preOrder(node){
        if(node){
            this.result.push(node.data);
            this.preOrder(node.left);
            this.preOrder(node.right);
        }
    }

    inOrder(node){
        if(node){
            this.inOrder(node.left);
            this.result.push(node.data);
            this.inOrder(node.right);
        }
    }

    postOrder(node){
        if(node){
            this.postOrder(node.left);
            this.postOrder(node.right);
            this.result.push(node.data);
        }
    }
}