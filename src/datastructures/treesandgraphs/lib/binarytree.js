export class BinaryTree {
    constructor(data) {
        this.left = null;
        this.right = null;
        this.parent = null;
        this.data = data;
        this.queue = [this];
    }

    add(value) {
        let current = this.queue[0];
        const next = new BinaryTree(value);
        next.parent = current;
        this.queue.push(next);
        if(!current.left) {
            current.left = next;
        } else {
            current.right = next;
            this.queue.splice(0,1);
        }
    }

}
