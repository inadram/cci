import { DirectedGraph} from './directedgraph';

describe('directed graph', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new DirectedGraph();
    });

    /*
        a --> b <----
         \--> c ----/
     */
    it('should have correct connection', () => {
        // given
        testSubject.addVertex('a');
        testSubject.addVertex('b');
        testSubject.addVertex('c');
        testSubject.addEdge('a', 'b');
        testSubject.addEdge('a', 'c');
        testSubject.addEdge('c', 'b');

        // when
        const nodeA = testSubject.nodes.get('a');
        const nodeB = testSubject.nodes.get('b');
        const nodeC = testSubject.nodes.get('c');

        // then
        expect(nodeA.outs.has('b')).toBe(true);
        expect(nodeA.outs.has('c')).toBe(true);
        expect(nodeC.outs.has('b')).toBe(true);
        expect(nodeB.outs.has('a')).toBe(false);
        expect(nodeB.outs.has('c')).toBe(false);
    });

    it('remove node should cut edges', () => {
        // Given
        testSubject.addVertex('a');
        testSubject.addVertex('b');
        testSubject.addVertex('c');
        testSubject.addEdge('a', 'b');
        testSubject.addEdge('a', 'c');
        testSubject.addEdge('c', 'b');

        // when
        testSubject.removeVertex('a');

        // then
        const nodeA = testSubject.nodes.get('a');
        const nodeB = testSubject.nodes.get('b');
        const nodeC = testSubject.nodes.get('c');
        expect(nodeA).toBeUndefined();
        expect(nodeA).toBeUndefined();
        expect(nodeC.outs.has('b')).toBe(true);
        expect(nodeB.outs.has('a')).toBe(false);
        expect(nodeB.outs.has('c')).toBe(false);
    })

    it('remove edge should cut connection', () => {
        // Given
        testSubject.addVertex('a');
        testSubject.addVertex('b');
        testSubject.addVertex('c');
        testSubject.addEdge('a', 'b');
        testSubject.addEdge('b', 'a');
        testSubject.addEdge('a', 'c');
        testSubject.addEdge('c', 'b');

        // when
        testSubject.removeEdge('a', 'b');

        // then
        const nodeA = testSubject.nodes.get('a');
        const nodeB = testSubject.nodes.get('b');
        const nodeC = testSubject.nodes.get('c');

        expect(nodeA.outs.has('b')).toBe(false);
        expect(nodeA.outs.has('c')).toBe(true);
        expect(nodeC.outs.has('b')).toBe(true);
        expect(nodeB.outs.has('a')).toBe(true);
        expect(nodeB.outs.has('c')).toBe(false);
    })
});