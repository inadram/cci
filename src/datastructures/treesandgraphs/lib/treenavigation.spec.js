import { TreeNavigation } from './treenavigation';
import { BinarySearchTree } from './binarysearchtree';

describe('tree navigation', () => {
    let testSubject, tree;
    /*
                 100
         50                    150
      25     75              110     250
                 90       105            300
                             107             350
                                108         320
     */

    beforeEach(() => {
        testSubject = new TreeNavigation();
        tree = new BinarySearchTree(100);
        tree.add(50);
        tree.add(150);
        tree.add(25);
        tree.add(75);
        tree.add(110);
        tree.add(250);
        tree.add(90);
        tree.add(105);
        tree.add(300);
        tree.add(107);
        tree.add(350);
        tree.add(108);
        tree.add(320);
    });

    it('post order', () => {
        // given
        let expected = [ 25, 90, 75, 50, 108, 107, 105, 110, 320, 350, 300, 250, 150, 100 ];
        // when
        testSubject.postOrder(tree);

        //then
        expect(testSubject.result).toEqual(expected)
    });

    it('in order', () => {
        // given
        let expected = [ 25, 50, 75, 90, 100, 105, 107, 108, 110, 150, 250, 300, 320, 350 ];
        // when
        testSubject.inOrder(tree);

        //then
        expect(testSubject.result).toEqual(expected)
    });

    it('pre order', () => {
        // given
        let expected = [ 100, 50, 25, 75, 90, 150, 110, 105, 107, 108, 250, 300, 350, 320 ];
        // when
        testSubject.preOrder(tree);

        //then
        expect(testSubject.result).toEqual(expected)
    });

});