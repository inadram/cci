export class BinarySearchTree {
    constructor(data) {
        this.left = null;
        this.right = null;
        this.data = data;
    }

    add(value) {
        const addLeft = (value) => {
            if (this.left) {
                this.left.add(value);
            } else {
                this.left = new BinarySearchTree(value);
                this.left.parent = this;
            }
        };

        const addRight = (value) => {
            if (this.right) {
                this.right.add(value);
            } else {
                this.right = new BinarySearchTree(value);
                this.right.parent = this;
            }
        };

        if (value < this.data) {
            addLeft(value);
        } else {
            addRight(value);
        }
    }
}
