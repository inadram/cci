import { BinaryTree } from './binarytree';

describe('binary tree', () => {
    let testSubject;

    /*
                10
           20         30
        40    50    60   70
      80
     */
    beforeEach(() => {
        testSubject = new BinaryTree(10);
    });

    it('add to the right place', () => {
        // given
        testSubject.add(20);
        testSubject.add(30);
        testSubject.add(40);
        testSubject.add(50);
        testSubject.add(60);
        testSubject.add(70);
        testSubject.add(80);

        // then
        expect(testSubject.data).toEqual(10);
        expect(testSubject.left.data).toEqual(20);
        expect(testSubject.right.data).toEqual(30);
        expect(testSubject.left.left.data).toEqual(40);
        expect(testSubject.left.right.data).toEqual(50);
        expect(testSubject.right.left.data).toEqual(60);
        expect(testSubject.right.right.data).toEqual(70);
        expect(testSubject.left.left.left.data).toEqual(80);
    });

    /*
                10
           20         30
        40    50    60   70
      80
     */

    it('set right parent', () => {
        // given
        testSubject.add(20);
        testSubject.add(30);
        testSubject.add(40);
        testSubject.add(50);
        testSubject.add(60);
        testSubject.add(70);
        testSubject.add(80);

        // then
        expect(testSubject.left.left.left.parent.data).toEqual(40);
        expect(testSubject.left.left.left.parent.parent.data).toEqual(20);
        expect(testSubject.left.left.left.parent.parent.parent.data).toEqual(10);
    });
});