import { IsSubtree } from './issubtree';
import { BinaryTree } from '../lib/binarytree';

describe('is subtree', () => {
    let testSubject, t1, t2;
    /*
                 10
              11            12
          13     14     15  16
        17 18   19 20
     */
    beforeEach(() => {
        t1 = new BinaryTree(10);
        t1.add(11);
        t1.add(12);
        t1.add(14);
        t1.add(15);
        t1.add(16);
        t1.add(17);
        t2 = new BinaryTree(13);
        t2.add(17);
        testSubject = new IsSubtree();
    });

    describe('check with dfs', () => {
        it('true when is subtree', () => {
            // when
            const result = testSubject.check(t1, t1.left.left);

            //then
            expect(result).toBeTruthy();
        });

        it('false when is not subtree', () => {
            // when
            const result = testSubject.check(t1, t2);

            //then
            expect(result).toBeFalsy();
        })
    });

    describe('check with bfs', () => {
        it('true when is subtree', () => {
            // when
            const result = testSubject.checkBfs(t1, t1.left.left);

            //then
            expect(result).toBeTruthy();
        });

        it('false when is not subtree', () => {
            // when
            const result = testSubject.checkBfs(t1, t2);

            //then
            expect(result).toBeFalsy();
        })
    });

});