export function IsSubtree() {
    function exactMatch(node1, node2) {
        if (!node1 && !node2) {
            return true;
        } else {
            if (( !node1 && node2 ) || ( !node2 && node1 ) || ( node1.data !== node2.data )) {
                return false;
            }
            return exactMatch(node1.left, node2.left) && exactMatch(node1.right, node2.right);
        }
    }

    function check(t1, t2) {
        if (!t1) {
            return false;
        } else {
            if (t1.data === t2.data && exactMatch(t1, t2)) {
                return true;
            }
            return check(t1.left, t2) || check(t1.right, t2);
        }
    }

    function checkBfs(t1, t2) {
        let queue = [t1];
        let found = false;
        while (queue.length >0 && !found) {
                let current = queue.shift();
                if(current.data === t2.data && exactMatch(current, t2)) {
                    found = true;
                }
                if(current.left) {
                    queue.push(current.left);
                }
                if(current.right) {
                    queue.push(current.right);
                }
        }
        return found;
    }

    return {
        check: (t1, t2) => check(t1, t2),
        checkBfs: (t1, t2) => checkBfs(t1, t2)
    }
}