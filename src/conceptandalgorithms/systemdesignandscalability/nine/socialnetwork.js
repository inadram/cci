export const SocialNetWork = () => {
    let users = new Map();

    function User(name) {
        this.name = name;
        this.path = [ [ name ] ];
        this.friends = new Set();

        this.addFriend = (name) => {
            this.friends.add(name);
        }
    }

    function addUser(name) {
        users.set(name, new User(name));
    }

    function getUser(name) {
        return users.get(name);
    }

    function addFriend(user, friend) {
        users.get(user).addFriend(friend);
        users.get(friend).addFriend(user);
    }

    function getFriends(user) {
        return users.get(user).friends;
    }

    function findShortestPath(user1, user2) {
        let visited = [];
        let paths = [ [ user1 ] ];
        let result = [];
        let queue = [ user1 ];
        while (queue.length) {
            let current = queue.shift();
            visited.push(current);
            let friends = Array.from(users.get(current).friends.values()).filter(value => !visited.includes(value));
            let partial = [];
            partial = paths.filter(( value => value[ value.length - 1 ] !== current ));
            let filtered = paths.filter(( value => value[ value.length - 1 ] === current ));
            friends.forEach((friend) => filtered.forEach(fil => partial.push(fil.concat(friend))));
            paths = partial;
            if (friends.includes(user2)) {
                filtered.map(fil => fil.push(user2));
                result = result.concat(filtered);
            } else {
                queue = queue.concat(friends);
            }
        }
        return result.reduce((left, right) => ( left.length < right.length ) ? left : right);
    }

    function findShortestPathWithPath(user1, user2) {
        let queue = [ user1 ];
        let result = [];
        let visited = [];
        while (queue.length) {
            let current = users.get(queue.shift());
            visited.push(current.name);
            current.friends.forEach((value) => {
                const friend = users.get(value);
                friend.path = current.path.map((each) => each.concat(friend.name));
            });
            if (current.friends.has(user2)) {
                const items = current.path.map(each => each.concat(user2));
                items.forEach(item => result.push(item));
            }
            queue = queue.concat(Array.from(current.friends.values()).filter(value => !visited.includes(value)));
        }
        return result.reduce((left, right) => ( left.length < right.length ) ? left : right);
    }

    function findShortestPathBothSideWithPath(user1, user2) {
        let leftVisited = [ user1 ];
        let rightVisited = [ user2 ];
        let leftQueue = [ user1 ];
        let rightQueue = [ user2 ];
        let result = [];

        function findPaths(leftFriends, leftNode, rightVisited) {
            let partialResult = [];
            leftFriends.filter(value => rightVisited.includes(value))
                .forEach((right) => {
                    leftNode.path.forEach(lPath => {
                        users.get(right).path.forEach(rPath => {
                            partialResult.push(lPath.concat(rPath))
                        });
                    })
                });
            return partialResult;
        }

        function addToQueue(friends, node, visited) {
            let queue = [];
            let notVisitedFriend = friends.filter(( value => !visited.includes(value) ));
            notVisitedFriend.forEach(friend => {
                let friendNode = users.get(friend);
                friendNode.path = node.path.map(node => node.concat(friend));
                queue.push(friend);
            });
            return queue;
        }

        while (leftQueue.length && rightQueue.length) {
            let leftValue = leftQueue.shift();
            leftVisited.push(leftValue);
            let leftNode = this.users.get(leftValue);
            let leftFriends = Array.from(leftNode.friends.values());
            result = result.concat(findPaths(leftFriends, leftNode, rightVisited));
            leftQueue = leftQueue.concat(addToQueue(leftFriends, leftNode, leftVisited));

            let rightValue = rightQueue.shift();
            rightVisited.push(rightValue);
            let rightNode = this.users.get(rightValue);
            let rightFriends = Array.from(rightNode.friends.values());
            result = result.concat(findPaths(rightFriends, rightNode, leftVisited));
            rightQueue = rightQueue.concat(addToQueue(rightFriends, rightNode, rightVisited));
        }

        return result.reduce((left, right) => ( left.length < right.length ) ? left : right);
    }

    return {
        addUser,
        addFriend,
        getFriends,
        getUser,
        findShortestPath,
        findShortestPathWithPath,
        findShortestPathBothSideWithPath,
        users
    }
};