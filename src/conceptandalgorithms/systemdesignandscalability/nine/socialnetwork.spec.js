import { SocialNetWork } from './socialnetwork';

describe('social network', () => {
    let testSubject;
    beforeEach(() => {
        testSubject = new SocialNetWork();
    });

    it('add/get user', () => {
        // given
        testSubject.addUser('amir');
        testSubject.addUser('amir2');
        // when
        const result = testSubject.getUser('amir');

        // then
        expect(result.name).toEqual('amir');
    });

    it('add/get friends', () => {
        // given
        testSubject.addUser('amir');
        testSubject.addUser('amir2');
        testSubject.addUser('amir3');
        testSubject.addUser('amir4');
        testSubject.addUser('amir5');
        testSubject.addFriend('amir', 'amir2');
        testSubject.addFriend('amir', 'amir3');
        testSubject.addFriend('amir', 'amir4');
        testSubject.addFriend('amir4', 'amir5');

        // when
        const result = testSubject.getFriends('amir');
        // then
        expect(Array.from(result.valueOf())).toEqual(['amir2', 'amir3', 'amir4']);
    });
    /*
            a -----> a2 ----> a8 ->
              ->a3-> a5->a6-> a7
                     a4
     */
    it('find shortest path between two nodes', () => {
        // given
        testSubject.addUser('amir');
        testSubject.addUser('amir2');
        testSubject.addUser('amir3');
        testSubject.addUser('amir4');
        testSubject.addUser('amir5');
        testSubject.addUser('amir6');
        testSubject.addUser('amir7');
        testSubject.addUser('amir8');
        testSubject.addFriend('amir', 'amir2');
        testSubject.addFriend('amir', 'amir3');
        testSubject.addFriend('amir2', 'amir8');
        testSubject.addFriend('amir8', 'amir7');
        testSubject.addFriend('amir3', 'amir5');
        testSubject.addFriend('amir3', 'amir4');
        testSubject.addFriend('amir5', 'amir6');
        testSubject.addFriend('amir6', 'amir7');

        // when
        const result = testSubject.findShortestPath('amir', 'amir7');

        // then
        expect(result).toEqual([ 'amir', 'amir2', 'amir8', 'amir7' ]);
    });

    it('find shortest path between two nodes with pereserving path', () => {
        // given
        testSubject.addUser('amir');
        testSubject.addUser('amir2');
        testSubject.addUser('amir3');
        testSubject.addUser('amir4');
        testSubject.addUser('amir5');
        testSubject.addUser('amir6');
        testSubject.addUser('amir7');
        testSubject.addUser('amir8');
        testSubject.addFriend('amir', 'amir2');
        testSubject.addFriend('amir', 'amir3');
        testSubject.addFriend('amir2', 'amir8');
        testSubject.addFriend('amir8', 'amir7');
        testSubject.addFriend('amir3', 'amir5');
        testSubject.addFriend('amir3', 'amir4');
        testSubject.addFriend('amir5', 'amir6');
        testSubject.addFriend('amir6', 'amir7');

        // when
        const result = testSubject.findShortestPathWithPath('amir', 'amir7');

        // then
        expect(result).toEqual([ 'amir', 'amir2', 'amir8', 'amir7' ]);
    });

    it('find shortest path between two nodes from both sides with pereserving path', () => {
        // given
        testSubject.addUser('amir');
        testSubject.addUser('amir2');
        testSubject.addUser('amir3');
        testSubject.addUser('amir4');
        testSubject.addUser('amir5');
        testSubject.addUser('amir6');
        testSubject.addUser('amir7');
        testSubject.addUser('amir8');
        testSubject.addFriend('amir', 'amir2');
        testSubject.addFriend('amir', 'amir3');
        testSubject.addFriend('amir2', 'amir8');
        testSubject.addFriend('amir8', 'amir7');
        testSubject.addFriend('amir3', 'amir5');
        testSubject.addFriend('amir3', 'amir4');
        testSubject.addFriend('amir5', 'amir6');
        testSubject.addFriend('amir6', 'amir7');

        // when
        const result = testSubject.findShortestPathBothSideWithPath('amir', 'amir7');

        // then
        expect(result).toEqual([ 'amir', 'amir2', 'amir8', 'amir7' ]);
    })
});