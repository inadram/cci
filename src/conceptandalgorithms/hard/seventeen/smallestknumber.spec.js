import { SmallestKNumber } from './smallestknumber';

describe('find', () => {
    let testSubject;
    let array;
    beforeEach(() => {
        array = new Array(10000).fill(0);
        array.forEach((value, index) => array[ index ] = index % 100 );

        testSubject = new SmallestKNumber(array);
    });
    it('findByInsertionSortInPlace smallest k number', () => {
        // when
        const result = testSubject.findByInsertionSortInPlace(101);

        // then
        expect(result).toBe(1);
    });

    it('findByInsertionSort smallest k number', () => {
        // when
        const result = testSubject.findByInsertionSort(101);

        // then
        expect(result).toBe(1);
    });

    it('findBySelectionSort smallest k number', () => {
        // when
        const result = testSubject.findBySelectionSort(101);

        // then
        expect(result).toBe(1);
    });

    it('findByBubbleSort smallest k number', () => {
        // when
        const result = testSubject.findByBubbleSort(101);

        // then
        expect(result).toBe(1);
    });

    it('findByMergeSort smallest k number', () => {
        // when
        const result = testSubject.findByMergeSort(101);

        // then
        expect(result).toBe(1);
    });

    it('findByQuickSort smallest k number', () => {
        // when
        const result = testSubject.findByQuickSort(101);

        // then
        expect(result).toBe(1);
    });

    it('findByHeapSort', () => {
        // when
        const result = testSubject.findByHeapSort(101);

        // then
        expect(result).toBe(1);

    });

    it('findBySelection', () => {
        // when
        const result = testSubject.findBySelection(101);

        // then
        expect(result).toBe(1);
    })
});
