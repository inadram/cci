export function SmallestKNumber(array) {

    function findByInsertionSort(k) {
        let result = [];
        for (let i = 0; i < array.length; i++) {
            let position = 0;
            while (position < result.length && result[ position ] < array[ i ]) {
                position++;
            }
            result.splice(position, 0, array[ i ]);
        }
        return result[ k - 1 ];
    }

    function findByInsertionSortInPlace(k) {
        for (let i = 0; i < array.length; i++) {
            let position = i;
            let current = array.splice(position, 1)[ 0 ];
            while (position > 0 && current < array[ position - 1 ]) {
                position--;
            }
            array.splice(position, 0, current);
        }
        return array[ k - 1 ];
    }

    function findBySelectionSort(k) {
        function findMin(index) {
            let minValue = Number.MAX_SAFE_INTEGER;
            let minIndex = -1;
            for (let i = index; i < array.length; i++) {
                if (array[ i ] < minValue) {
                    minValue = array[ i ];
                    minIndex = i;
                }
            }
            return minIndex;
        }

        for (let i = 0; i < array.length; i++) {
            let minIndex = findMin(i);
            swap(i, minIndex);
        }

        return array[ k - 1 ];
    }

    function findByBubbleSort(k) {
        let unSorted = true;
        while (unSorted) {
            unSorted = false;
            for (let i = 0; i < array.length - 1; i++) {
                if (array[ i + 1 ] < array[ i ]) {
                    swap(i, i + 1);
                    unSorted = true;
                }
            }
        }
        return array[ k - 1 ];
    }

    function swap(left, right) {
        let temp = array[ right ];
        array[ right ] = array[ left ];
        array[ left ] = temp;
    }

    function findByMergeSort(k) {
        function sort(array, leftStart, leftEnd, rightStart, rightEnd) {
            while (leftStart <= leftEnd) {
                while (rightStart < rightEnd && array[ leftStart ] > array[ rightStart ]) {
                    swap(leftStart, rightStart);
                    rightStart++;
                }
                leftStart++;
            }
        }

        function merge(array, left, right) {
            if (left < right) {
                let mid = Math.floor(( left + right ) / 2);
                merge(array, left, mid);
                merge(array, mid + 1, right);
                sort(array, left, mid - 1, mid, right);
            }
        }

        merge(array, 0, array.length - 1);
        return array[ k - 1 ];
    }

    function findByQuickSort(k) {
        function quickSort(array, left, right) {
            function sort(array, pivot, left, right) {
                while (left < right) {
                    while (array[ left ] < pivot) {
                        left++;
                    }
                    while (array[ right ] > pivot) {
                        right--;
                    }
                    if (left < right) {
                        swap(left, right);
                        left++;
                        right--;
                    }
                }
                return left;
            }

            if (left < right) {
                let mid = Math.floor(( left + right ) / 2);
                let index = sort(array, array[ mid ], left, right);
                quickSort(array, left, index - 1);
                quickSort(array, index + 1, right);
            }
        }

        quickSort(array, 0, array.length - 1);

        return array[ k - 1 ];
    }

    /*
            0
         1      2
       3   4  5   6
     */
    function findByHeapSort(k) {
        function moveDown(index, array) {
            if (index < array.length - 1) {
                const children = [ ( index + 1 ) * 2, ( ( index + 1 ) * 2 ) - 1 ];
                const minChildId = children.reduce((l, r) => ( array[ l ] < array[ r ] ) ? l : r);
                if (array[ minChildId ] < array[ index ]) {
                    array[ index ] += array[ minChildId ];
                    array[ minChildId ] = array[ index ] - array[ minChildId ];
                    array[ index ] = array[ index ] - array[ minChildId ];
                    moveDown(minChildId, array);
                }
            }
        }

        function minHeapify(array) {
            function moveUp(index, array) {
                if (index > 0) {
                    let parent = Math.floor(( index - 1 ) / 2);
                    if (array[ index ] < array[ parent ]) {
                        array[ parent ] = array[ index ] + array[ parent ];
                        array[ index ] = array[ parent ] - array[ index ];
                        array[ parent ] = array[ parent ] - array[ index ];
                        moveUp(parent, array);
                    }
                }
            }

            for (let i = array.length - 1; i >= 0; i--) {
                moveUp(i, array);
            }
        }

        minHeapify(array);
        const result = [];
        while (array.length) {
            result.push(array.shift());
            if (array.length) {
                const popped = array.pop();
                array.unshift(popped);
                moveDown(0, array);
            }
        }
        return result[ k - 1 ];
    }

    function findBySelection(k) {
        function findK(array, k) {
            let pivotValue = array[ Math.floor(array.length / 2) ];
            const divide = array.reduce((partial, value) => {
                if (value < pivotValue) {
                    partial[ 'left' ].push(value);
                }
                if (value > pivotValue) {
                    partial[ 'right' ].push(value);
                }
                if(value === pivotValue) {
                    partial['equal'].push(value);
                }
                return partial;
            }, { left: [], right: [] , equal:[]});

            if (k === divide[ 'left' ].length) {
                return pivotValue;
            } else if (divide[ 'left' ].length > k) {
                return findK(divide[ 'left' ], k);
            } else if (divide['left'].length + divide['equal'].length> k){
                return divide['equal'][0];
            }
            else {
                return findK(divide[ 'right' ], k - ( divide[ 'left' ].length + divide['equal'].length ));
            }
        }

        return findK(array, k-1);
    }

    return {
        findByInsertionSort,
        findByInsertionSortInPlace,
        findBySelectionSort,
        findByBubbleSort,
        findByMergeSort,
        findByQuickSort,
        findByHeapSort,
        findBySelection
    }
}