export const Binode = () => {
    /*
                           100
               50                      200
        10         80            150            250
      5   15    75   90       110    170     240
    1             78                       210
      */
    function Node(head, tail) {
        this.head = head;
        this.tail = tail;
    }

    const convertByLeftAndRight = (bst) => {
        byLeftAndRight(bst);
        return leftMost(bst);
    };

    function byLeftAndRight(bst) {
        if(bst){
            let left = byLeftAndRight(bst.left);
            let right = byLeftAndRight(bst.right);

            if(left){
                let rightMostNode = rightMost(left);
                rightMostNode.right = bst;
                bst.left = rightMostNode;
            }

            if(right){
                let leftMostNode = leftMost(right);
                leftMostNode.left = bst;
                bst.right = leftMostNode;
            }
            return bst;
        }
    }

    const convertWithLinkedList = (bst) => {
        byLinkedList(bst);
        return leftMost(bst);
    };

    function byLinkedList(bst) {
        if(bst) {
            let left = byLinkedList(bst.left);
            let right = byLinkedList(bst.right);
            if(right) {
                bst.right = right.head;
                right.head.left = bst;
            }
            if(left) {
                bst.left = left.tail;
                left.tail.right = bst;
            }
            return new Node((left)? left.head:bst, (right)?right.tail: bst);
        }
    }

    function inOrderNavigate(bst) {
        if(bst) {
            inOrderNavigate(bst.left);
            inOrderNavigate(bst.right);
            if(bst.left){
                let rightMostNode = rightMost(bst.left);
                rightMostNode.right = bst;
                bst.left = rightMostNode;
            }
            if(bst.right){
                let leftMostNode = leftMost(bst.right);
                leftMostNode.left = bst;
                bst.right = leftMostNode;
            }
        }
    }

    function rightMost(bst) {
        while (bst.right){
            bst = bst.right;
        }
        return bst;
    }

    function leftMost(bst) {
        while (bst.left){
            bst = bst.left;
        }
        return bst;
    }

    function convertInPlace(bst) {
        inOrderNavigate(bst);
        return leftMost(bst);
    }

    function convert(bst) {
        let queue = inOrder(bst, []);
        for (let i = 0; i < queue.length - 1; i++) {
            queue[ i ].right = queue[ i + 1 ];
            queue[ i + 1 ].left = queue[ i ];
        }
        return queue[0];
    }

    function inOrder(bst, queue) {
        if (!bst) {
            return queue;
        }
        queue = inOrder(bst.left, queue);
        queue.push(bst);
        queue = inOrder(bst.right, queue);
        return queue;
    }

    return { convert, convertInPlace, convertWithLinkedList, convertByLeftAndRight }
};