import { BinarySearchTree } from '../../../datastructures/treesandgraphs/lib/binarysearchtree';
import { Binode } from './binode';

describe('convert binode to tree', () => {
    let testSubject;
    let values = [ 50, 200, 10, 80, 150, 250, 5, 15, 75, 90, 110, 170, 240, 1, 78, 210 ];
    beforeEach(() => {
        testSubject = new Binode();
    });
    /*
                          100
              50                      200
       10         80            150            250
     5   15    75   90       110    170     240
   1             78                       210
     */
    it('convert', () => {
        // given
        let bst = createBst();
        let expected = [ 1, 5, 10, 15, 50, 75, 78, 80, 90, 100, 110, 150, 170, 200, 210, 240, 250 ];
        // when
        let result = testSubject.convert(bst);
        // then
        expected.forEach(value => {
            expect(result.data).toBe(value);
            result = result.right
        })
    });

    it('convert in-place', () => {
        // given
        let bst = createBst();
        let expected = [ 1, 5, 10, 15, 50, 75, 78, 80, 90, 100, 110, 150, 170, 200, 210, 240, 250 ];
        // when
        let result = testSubject.convertInPlace(bst);
        // then
        expected.forEach(value => {
            expect(result.data).toBe(value);
            result = result.right
        })
    });

    it('convert by linked list', () => {
        // given
        let bst = createBst();
        let expected = [ 1, 5, 10, 15, 50, 75, 78, 80, 90, 100, 110, 150, 170, 200, 210, 240, 250 ];
        // when
        let result = testSubject.convertWithLinkedList(bst);
        // then
        expected.forEach(value => {
            expect(result.data).toBe(value);
            result = result.right
        })
    });

    it('convert by left right', () => {
        // given
        let bst = createBst();
        let expected = [ 1, 5, 10, 15, 50, 75, 78, 80, 90, 100, 110, 150, 170, 200, 210, 240, 250 ];
        // when
        let result = testSubject.convertByLeftAndRight(bst);
        // then
        expected.forEach(value => {
            expect(result.data).toBe(value);
            result = result.right
        })
    });

    function createBst() {
        let bst = new BinarySearchTree(100);
        values.forEach(value => bst.add(value));
        return bst;
    }

});
