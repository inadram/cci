import { CommonName } from './commonname';

describe('find common name', () => {
   let testSubject;
   beforeEach(()=>{
       testSubject = new CommonName();
   });
/*
	input:   Amir(10), Amir2(20), inadram(15), rima (30)
		    (Amir, Amir2), (Amir2, inadram)
	result: Amir(45), rima(30)
 */
    it('common name', ()=>{
        let names = new Map();
        names.set('Amir', 10);
        names.set('Amir2', 20);
        names.set('inadram', 15);
        names.set('rima', 30);
        let synonym = new Map();
        synonym.set('Amir', 'Amir2');
        synonym.set('Amir2', 'inadram');
       const result = testSubject.find(names, synonym);

       expect(result.get('Amir')).toEqual(45);
       expect(result.get('rima')).toEqual(30);
    });

    it('common name  by map', ()=>{
        let names = new Map();
        names.set('Amir', 10);
        names.set('Amir2', 20);
        names.set('inadram', 15);
        names.set('rima', 30);
        let synonym = new Map();
        synonym.set('Amir', 'Amir2');
        synonym.set('Amir2', 'inadram');
        const result = testSubject.findByMap(names, synonym);

        expect(result.get('Amir')).toEqual(45);
        expect(result.get('rima')).toEqual(30);
    });

    it('common name by merge map', ()=>{
        let names = new Map();
        names.set('Amir', 10);
        names.set('Amir2', 20);
        names.set('inadram', 15);
        names.set('rima', 30);
        let synonym = new Map();
        synonym.set('Amir', 'Amir2');
        synonym.set('inadram', 'Amir2');
        const result = testSubject.findByMergeMap(names, synonym);

        expect(result.get('Amir')).toEqual(45);
        expect(result.get('rima')).toEqual(30);
    });
});