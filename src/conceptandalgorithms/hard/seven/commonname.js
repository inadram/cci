export class CommonName {
    constructor() {
        this.visited = [];
    }

    find(names, synonym) {
        let graph = this.buildGraph(names, synonym);
        return this.navigate(graph);
    }

    navigate(graph) {
        let result = new Map();
        graph.nodes.forEach((value, key, map) => {
            const partialValue = this.navigateEachRec(key, map);
            if (partialValue > 0) {
                result.set(key, partialValue);
            }
        });
        return result;
    }

    navigateEach(nodeName, map) {
        let partial = 0;
        if (!this.visited.includes(nodeName)) {
            let queue = [ nodeName ];
            while (queue.length) {
                let current = map.get(queue.shift());
                this.visited.push(current.name);
                partial += current.value;
                current.synonims.filter(value => !this.visited.includes(value)).forEach(notVisited => queue.push(notVisited));
            }
        }
        return partial;
    }

    navigateEachRec(nodeName, map) {
        let partial = 0;
        if (!this.visited.includes(nodeName)) {
            let node = map.get(nodeName);
            partial = node.synonims
                .filter(value => !this.visited.includes(value))
                .reduce((partial, value) => partial + this.navigateEachRec(value, map), node.value);
        }
        return partial;
    }

    buildGraph(names, synonym) {
        let graph = new Graph();
        names.forEach((value, key) => graph.addNode(key, value));
        synonym.forEach((value, key) => {
            let node = graph.getNode(key);
            if (!node.synonims.includes(value)) {
                node.addNeighbour(value);
            }
        });
        return graph;
    }

    findByMap(names, synonym) {
        let allSynonyms = new Map();
        let result = new Map();
        synonym.forEach((value, key) => {
            let allSynonymsValues = Array.from(allSynonyms.values()).reduce((partial, value) => partial.concat(Array.from(value)), []);
            if (!allSynonymsValues.includes(key) && !allSynonymsValues.includes(value)) {
                const values = new Set();
                values.add(value);
                values.add(key);
                allSynonyms.set(key, values);
            } else {
                allSynonyms.forEach((synValue, synKey) => {
                    if (synValue.has(value) || synValue.has(key)) {
                        allSynonyms.get(synKey).add(value);
                        allSynonyms.get(synKey).add(key);
                    }
                });
            }
        });
        allSynonyms.forEach(( (value, key) => {
            let partial = Array.from(value.values()).reduce((partial, value) => partial + names.get(value), 0);
            result.set(key, partial);
        } ));
        let synNodes = [];
        allSynonyms.forEach(value => synNodes = synNodes.concat(Array.from(value.values())));
        let notSynNodeName = Array.from(names.keys()).filter(name => !synNodes.includes(name));
        notSynNodeName.forEach(value => result.set(value, names.get(value)));
        return result;
    }

    findByMergeMap(names, synonym) {
        let result = new Map();
        let group = new Map();
        names.forEach((value, key) => {
            let set = new Set();
            set.add(key);
            group.set(key, set);
        });
        synonym.forEach((value, key) => {
            let left = group.get(value);
            let right = group.get(key);
            left.forEach(leftValue => right.add(leftValue));
            right.forEach(rightValue => left.add(rightValue));
            group.set(value, right);
        });
        group.forEach((value, key)=>{
            let sum = Array.from(value).reduce((partial, name)=> partial + names.get(name), 0);
            result.set(key, sum);
        });
        return result;
    }
}

class Graph {
    constructor() {
        this.nodes = new Map();
    }

    addNode(name, value) {
        this.nodes.set(name, new Node(name, value))
    }

    getNode(name) {
        return this.nodes.get(name);
    }

    addNeighbour(name, neighbour) {
        this.nodes.get(name).addNeighbour(neighbour);
        this.nodes.get(neighbour).addNeighbour(name);
    }
}

class Node {
    constructor(name, value) {
        this.synonims = [];
        this.name = name;
        this.value = value;
    }

    addNeighbour(name) {
        this.synonims.push(name);
    }

}