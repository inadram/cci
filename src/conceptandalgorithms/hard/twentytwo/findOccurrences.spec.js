import { FindOccurrences } from './findOccurrences';

describe('find occurrences',()=>{
    let testSubject;
    beforeEach(()=>{
       testSubject = new FindOccurrences();
    });

    it('find with search', () => {
        // given
        const string = 'thethisisismiss';
        const array = ['this','is','miss','the','iss'];

        // when
        const result = testSubject.findWithSearch(string, array);

        // then
        expect(result['this']).toBe(1);
        expect(result['the']).toBe(1);
        expect(result['is']).toBe(4);
        expect(result['miss']).toBe(1);
        expect(result['iss']).toBe(1);
    });
    it('find with split', () => {
        // given
        const string = 'thethisisismiss';
        const array = ['this','is','miss','the','iss'];

        // when
        const result = testSubject.findWithSplit(string, array);

        // then
        expect(result['this']).toBe(1);
        expect(result['the']).toBe(1);
        expect(result['is']).toBe(4);
        expect(result['miss']).toBe(1);
        expect(result['iss']).toBe(1);
    });
    it('find with trie', () => {
        // given
        const string = 'thethisisismiss';
        const array = ['this','is','miss','the','iss'];

        // when
        const result = testSubject.findWithTrie(string, array);

        // then
        expect(result['this']).toBe(1);
        expect(result['the']).toBe(1);
        expect(result['is']).toBe(4);
        expect(result['miss']).toBe(1);
        expect(result['iss']).toBe(1);
    });
});