export function FindOccurrences() {
    function findWithSearch(string, array) {
        let result = [];
        for (let arr of array) {
            for (let i = 0; i < string.length; i++) {
                if (arr[ 0 ] === string[ i ]) {
                    let identical = true;
                    for (let index = 0; index < arr.length; index++) {
                        if (arr[ index ] !== string[ i + index ]) {
                            identical = false;
                            break;
                        }
                    }
                    if (identical) {
                        if (!result[ arr ]) {
                            result[ arr ] = 0;
                        }
                        result[ arr ]++;
                    }
                }
            }
        }
        return result;
    }

    function findWithSplit(string, array) {
        let result = [];
        for (let arr of array) {
            const partial = string.split(arr);
            result[ arr ] = partial.length - 1;
        }
        return result;
    }

    function findWithTrie(string, array) {
        let graph = [];

        function trie(value) {
            let children = [];
            let data = value;
            let type = null;

            function addChildren(value) {
                children.push(value);
            }

            return {
                addChildren, type, data, children
            }
        }

        for (let arr of array) {
            for (let i = 0; i < arr.length - 1; i++) {
                if (!graph[ arr[ i ] ]) {
                    graph[ arr[ i ] ] = trie(arr[ i ]);
                }
                if (!graph[ arr[ i + 1 ] ]) {
                    graph[ arr[ i + 1 ] ] = trie(arr[ i + 1 ]);
                }
                graph[ arr[ i ] ].addChildren(arr[ i + 1 ]);
            }
            graph[ arr[ 0 ] ].type = 'start';
            graph[ arr[ arr.length - 1 ] ].type = 'end';
        }

        let result = [];

        function findPath(children, string, index, partial) {
            if (index < string.length && children.includes(string[ index ])) {
                const graphElement = graph[ string[ index ] ];
                partial = partial.map(value => value.concat(graphElement.data));
                if (graphElement.type === 'start') {
                    partial.push(graphElement.data);
                }
                if (graphElement.type === 'end') {
                    const possibles = partial.filter(value => array.includes(value));
                    possibles.forEach(value => {
                        if (result[value]) {
                            result[ value ] += 1;
                        } else {
                            result[ value ] = 1;
                        }
                    });
                }
            } else {
                return index;
            }
            return findPath(graph[ string[ index ] ].children, string, index + 1, partial);
        }

        function findNode(graph, string, index) {
            if (index < string.length) {
                const graphElement = graph[ string[ index ] ];
                index++;
                if (graphElement && graphElement.type === 'start') {
                    const children = graphElement.children;
                    index = findPath(children, string, index, [ graphElement.data ]);
                }
                findNode(graph, string, index);
            }
        }

        findNode(graph, string, 0);
        return result;
    }

    return { findWithSearch, findWithSplit, findWithTrie }
}