export class FindWords {
    constructor() {
    }

    findWithCache(input, words) {
        return this.findPartialWithCache(input, words, 0, 0, 0, Number.MAX_SAFE_INTEGER, new Map());
    }

    findPartialWithCache(input, words, start, end, currentSize, maxLeft, cache) {
        function clearCache(value, cacheName) {
            if (cache.has(cacheName)) {
                if (value < cache.get(cacheName)) {
                    cache.set(cacheName, value);
                }
            } else {
                cache.set(cacheName, value);
            }
        }

        if (start <= input.length) {
            while (end <= input.length) {
                end += 1;
                let possible = input.slice(start, end);
                if (words.includes(possible) && ( maxLeft > ( end - ( currentSize + possible.length ) ) )) {
                    if (cache.has(start)) {
                        maxLeft = cache.get(start);
                    } else {
                        maxLeft = this.findPartialWithCache(input, words, end, end, currentSize + possible.length, maxLeft, cache);
                    }
                }
            }
            let currentMaxLeft = input.length - currentSize;
            if (currentMaxLeft < maxLeft) {
                clearCache(currentMaxLeft, start);
                maxLeft = currentMaxLeft;
            }
            start++;

            maxLeft = this.findPartialWithCache(input, words, start, start, currentSize, maxLeft, cache);
        }
        return maxLeft;
    }

    findWithShortCircuit(input, words) {
        return this.findPartialWithShortCircuit(input, words, 0, 0, 0, Number.MAX_SAFE_INTEGER);
    }

    findPartialWithShortCircuit(input, words, start, end, currentSize, maxLeft) {
        if (start <= input.length) {
            while (end <= input.length) {
                end += 1;
                let possible = input.slice(start, end);
                if (words.includes(possible) && ( maxLeft > ( end - ( currentSize + possible.length ) ) )) {
                    maxLeft = this.findPartialWithShortCircuit(input, words, end, end + 1, currentSize + possible.length, maxLeft);
                }
            }
            start++;

            let currentMaxLeft = input.length - currentSize;
            if (currentMaxLeft < maxLeft) {
                maxLeft = currentMaxLeft;
            }
            maxLeft = this.findPartialWithShortCircuit(input, words, start, start, currentSize, maxLeft);
        }
        return maxLeft;
    }

    find(input, words) {
        let result = this.findPartial(input, words, []);
        result = result.reduce((left, right) => ( left.join('').length > right.join('').length ) ? left : right);
        return input.length - result.join('').length;
    }

    findPartial(input, words, partialResult) {
        let result = [];
        if (input.length) {
            for (let i = 0; i < input.length; i++) {
                let possibleWord = input.slice(0, i + 1);
                if (words.includes(possibleWord)) {
                    partialResult.push(possibleWord);
                    result = result.concat(this.findPartial(input.slice(i + 1), words, partialResult));
                    partialResult.pop();
                }
            }
            result = result.concat(this.findPartial(input.slice(1), words, partialResult));
        } else {
            result.push(partialResult.slice());
        }
        return result;
    }

    findWithRecursion(input, words) {
        function findPartialWithRecursion(input, words, index, cache) {
            let maxLeft = Number.MAX_SAFE_INTEGER;
            if (input.length <= index) {
                return 0;
            }
            for (let i = index; i < input.length; i++) {
                let possible = input.slice(index, i + 1);
                let possibleLeft = ( words.includes(possible) ) ? 0 : possible.length;
                if (possibleLeft < maxLeft) {
                    let prevMaxLeft = ( cache.has(i + 1) ) ? cache.get(i + 1) : findPartialWithRecursion(input, words, i + 1, cache);
                    let possibleMaxLeft = prevMaxLeft + possibleLeft;
                    if (possibleMaxLeft < maxLeft) {
                        cache.set(index, possibleMaxLeft);
                        maxLeft = possibleMaxLeft;
                    }
                }
            }
            return maxLeft;
        }

        return findPartialWithRecursion(input, words, 0, new Map());
    }
}
