import { FindWords } from './findwords';

describe('find words', () => {
    let testSubject;
    let words = [ 'is', 'dear', 'ok', 'oh', 'some', 'test', 'this', 'so', 'soo', 'sooo', 'soooo', 'hi', 'he', 'hello', 'is', 'iso', 'amir', 'am' ];

    beforeEach(() => {
        testSubject = new FindWords();
    });

    it('find', () => {
        // i soooo hello
        const result = testSubject.find('isoooohelloamir', words);
        //is,hello -oooo 4
        //iso, oh, -oo ello 6
        //iso, he, -ooo llo 6
        //iso, hello, -ooo 3
        //sooo,oh  -i ello 4
        //soo,oh  -i o ello 5
        //soooo,he -i  llo 4
        //soooo, hello -i 1
        expect(result).toBe(1)
    });

    it('find with short circuit', () => {
        // i soooo hello
        const result = testSubject.findWithShortCircuit('isoooohelloamir', words);
        expect(result).toBe(1)
    });

    it('find with cache', () => {
        // i soooo hello
        const result = testSubject.findWithCache('isoooohelloamir', words);
        expect(result).toBe(1)
    });

    it('find with return recursive', () => {
        // i soooo hello
        const result = testSubject.findWithRecursion('isoooohelloamir', words);
        expect(result).toBe(1)
    })
});