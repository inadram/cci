export class Paint {
    constructor() {
    }

    fillGraph(screen, x, y, color) {
        function fill(screenArray, x, y, newColor) {
            if (x < 0 || x >= screenArray.length || y < 0 || y >= screenArray[ 0 ].length) {
                return false;
            }
            if (screenArray[ x ][ y ] !== newColor) {
                screenArray[ x ][ y ] = newColor;
                fill(screenArray, x - 1, y, newColor);
                fill(screenArray, x, y - 1, newColor);
                fill(screenArray, x + 1, y, newColor);
                fill(screenArray, x, y + 1, newColor);
            }
        }

        return fill(screen, x, y, color);
    }

    fill(screen, x, y, color) {
        let startRow = Paint.getMinStart(x);
        let endRow = ( x < screen.length - 1 ) ? x + 1 : screen.length - 1;
        let startCol = Paint.getMinStart(y);
        let endCol = ( y < screen[ x ].length - 1 ) ? y + 1 : screen[ x ].length - 1;
        while (startRow > 0 && screen[ startRow ][ y ] === color) {
            startRow--;
        }
        while (endRow < screen.length && screen[ endRow ][ y ] === color) {
            endRow++;
        }
        while (startCol > 0 && screen[ x ][ startCol ] === color) {
            startCol--;
        }
        while (endCol < screen[ x ].length && screen[ x ][ endCol ] === color) {
            endCol++;
        }
        for (let row = startRow; row <= endRow; row++) {
            for (let col = startCol; col <= endCol; col++) {
                screen[ row ][ col ] = color;
            }
        }
        return screen;
    }

    static getMinStart(value) {
        return ( value > 0 ) ? value - 1 : 0;
    }
}