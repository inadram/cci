import { Paint } from './paint';

describe('paint', () => {
    let testSubject, screen;
    beforeEach(() => {
        testSubject = new Paint();
        screen = Array(10).fill('R').map(() => Array(10).fill('R'));

    });
    /*
            1  2  3  4  5  6  7  8  9  10
            11 12 13 14 15 16 17 18 19 20
            21 22 23 24 25 26 27 28 29 30
            31 32 33 34 35 36 37 38 39 40
            41 42 43 44 45 46 47 48 49 50
            41 42 43 44 45 46 47 48 49 50
            51 52 53 54 55 56 57 58 59 60
            61 62 63 64 65 66 67 68 69 70
            71 72 73 74 75 76 77 78 79 80
            81 82 83 84 85 86 87 88 89 90
            91 92 93 94 95 96 97 98 99 100
     */
    it('fill with array', () => {
        // given
        // when
        const result = testSubject.fill(screen, 4, 4, 'B');

        // then
        expect(result[ 3 ][ 3 ]).toBe('B');
        expect(result[ 3 ][ 4 ]).toBe('B');
        expect(result[ 3 ][ 5 ]).toBe('B');
        expect(result[ 4 ][ 3 ]).toBe('B');
        expect(result[ 4 ][ 4 ]).toBe('B');
        expect(result[ 4 ][ 5 ]).toBe('B');
        expect(result[ 5 ][ 3 ]).toBe('B');
        expect(result[ 5 ][ 4 ]).toBe('B');
        expect(result[ 5 ][ 5 ]).toBe('B');
        expect(result[ 2 ][ 3 ]).toBe('R');

        const result2 = testSubject.fill(result, 4, 4, 'B');
        expect(result2[ 2 ][ 2 ]).toBe('B');
        expect(result2[ 2 ][ 3 ]).toBe('B');
        expect(result2[ 2 ][ 4 ]).toBe('B');
        expect(result2[ 2 ][ 5 ]).toBe('B');
        expect(result2[ 2 ][ 6 ]).toBe('B');
        expect(result2[ 6 ][ 2 ]).toBe('B');
        expect(result2[ 6 ][ 3 ]).toBe('B');
        expect(result2[ 6 ][ 4 ]).toBe('B');
        expect(result2[ 6 ][ 5 ]).toBe('B');
        expect(result2[ 6 ][ 6 ]).toBe('B');
        expect(result2[ 6 ][ 7 ]).toBe('R');

        const result3 = testSubject.fill(result2, 0, 0, 'B');
        expect(result3[ 0 ][ 0 ]).toBe('B');
        expect(result3[ 0 ][ 1 ]).toBe('B');
        expect(result3[ 1 ][ 0 ]).toBe('B');
        expect(result3[ 1 ][ 1 ]).toBe('B');
        expect(result3[ 1 ][ 2 ]).toBe('R');
    });

    it('fill with graph', () => {
        // when
        testSubject.fillGraph(screen, 4, 4, 'B');
        // then
        expect(screen.every(row=> row.every(cell=> cell === 'B'))).toBeTruthy();
    });
});