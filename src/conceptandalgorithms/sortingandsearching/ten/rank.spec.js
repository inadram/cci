import Rank from './rank';

describe('rank', () => {
    let testSubject;
    //5,1,4,4,5,9,7,13,3
    describe('track', () => {
        beforeEach(() => {
            testSubject = new Rank();
            testSubject.add(1);
            testSubject.add(5);
            testSubject.add(4);
            testSubject.add(4);
            testSubject.add(5);
            testSubject.add(9);
            testSubject.add(7);
            testSubject.add(13);
            testSubject.add(3);
        });

        it('track rank 1', () => {
            // when
            const result = testSubject.getRank(1);

            //
            expect(result).toBe(0);
        });

        it('track rank 3', () => {
            // when
            const result = testSubject.getRank(3);

            //
            expect(result).toBe(1);
        });

        it('track rank 4', () => {
            // when
            const result = testSubject.getRank(4);

            //
            expect(result).toBe(3);
        })
    });

    describe('track with update', () => {
        beforeEach(() => {
            testSubject = new Rank();
            testSubject.addWithUpdate(1);
            testSubject.addWithUpdate(5);
            testSubject.addWithUpdate(4);
            testSubject.addWithUpdate(4);
            testSubject.addWithUpdate(5);
            testSubject.addWithUpdate(9);
            testSubject.addWithUpdate(7);
            testSubject.addWithUpdate(13);
            testSubject.addWithUpdate(3);
        });

        it('track rank 1', () => {
            // when
            const result = testSubject.getRankWithUpdate(1);

            //
            expect(result).toBe(0);
        });

        it('track rank 3', () => {
            // when
            const result = testSubject.getRankWithUpdate(3);

            //
            expect(result).toBe(1);
        });

        it('track rank 4', () => {
            // when
            const result = testSubject.getRankWithUpdate(4);

            //
            expect(result).toBe(3);
        })
    });

    describe('track with tree', () => {
        beforeEach(() => {
            testSubject = new Rank();
            testSubject.addWithTree(1);
            testSubject.addWithTree(5);
            testSubject.addWithTree(4);
            testSubject.addWithTree(4);
            testSubject.addWithTree(9);
            testSubject.addWithTree(7);
            testSubject.addWithTree(13);
            testSubject.addWithTree(3);
        });

        it('track rank 1', () => {
            // when
            const result = testSubject.getRankWithTree(1);

            //
            expect(result).toBe(0);
        });

        it('track rank 3', () => {
            // when
            const result = testSubject.getRankWithTree(3);

            //
            expect(result).toBe(1);
        });

        it('track rank 4', () => {
            // when
            const result = testSubject.getRankWithTree(4);

            //
            expect(result).toBe(3);
        });

        it('track rank 5', () => {
            // when
            const result = testSubject.getRankWithTree(5);

            //
            expect(result).toBe(4);
        });

        it('track rank 7', () => {
            // when
            const result = testSubject.getRankWithTree(7);

            //
            expect(result).toBe(5);
        });

        it('track rank 9', () => {
            // when
            const result = testSubject.getRankWithTree(9);

            //
            expect(result).toBe(6);
        });

    });
});