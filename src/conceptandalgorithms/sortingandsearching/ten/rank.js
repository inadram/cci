function Rank() {
    let numbers = [];
    let binaryTree;

    function add(x) {
        numbers[ x ] = ( numbers[ x ] ) ? numbers[ x ] + 1 : 1;
    }

    function getRank(x) {
        return numbers.slice(0, x + 1).reduce((left, right) => left + right, 0) - 1;
    }

    function addWithUpdate(x) {
        add(x);
        numbers[ x ] += numbers.filter((v, i) => i < x).reduce((l, r) => l + r, 0);
    }

    function getRankWithUpdate(x) {
        return numbers[ x ] - 1;
    }

    /*
                    1
                           5
                      4       9
                   3       7   13
     */
    function BinaryTree(data, rank = 1) {
        this.data = data;
        this.rank = rank;
        this.left = null;
        this.right = null;

        this.add = function(x) {
            if (x === this.data) {
                this.rank++;
            } else if (x < this.data) {
                this.rank++;
                if (this.left) {
                    this.left.add(x);
                } else {
                    this.left = new BinaryTree(x);
                }
            } else if (x > this.data) {
                if (this.right!== null) {
                    this.right.add(x);
                } else {
                    this.right = new BinaryTree(x);
                }
            }
        };

        this.get = function(x, rank = 0) {
            if (x === this.data) {
                return rank + this.rank;
            } else if (x < this.data) {
                return this.left.get(x, rank);
            } else {
                return this.right.get(x, rank + this.rank);
            }
        }
    }

    function addWithTree(x) {
        if (binaryTree) {
            binaryTree.add(x);
        } else {
            binaryTree = new BinaryTree(x)
        }
    }

    function getRankWithTree(x) {
        return binaryTree.get(x) - 1;
    }

    return { add, getRank, addWithUpdate, getRankWithUpdate, addWithTree, getRankWithTree }
}

export default Rank;